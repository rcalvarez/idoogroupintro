package com.example.idoogroup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.idoogroup.databaseManager.AndroidDatabaseManager;
import com.example.idoogroup.model.SQLiteHelper;
import com.example.idoogroup.model.User;
import com.example.idoogroup.ui.home.HomeFragment;
import com.example.idoogroup.ui.login.LoginActivity;
import com.example.idoogroup.ui.product.createEdit.CreateEditProductFragment;
import com.example.idoogroup.ui.product.list.ListProductFragment;
import com.example.idoogroup.ui.scan.ScanFragment;
import com.example.idoogroup.utils.SaveSharedPreference;
import com.google.android.material.navigation.NavigationView;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

@SuppressLint("StaticFieldLeak")
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    //index of views
    public static final int INDEX_HOME = 0;
    public static final int INDEX_FILMS = 1;
    public static final int INDEX_CREATE_PRODUCT = 2;
    public static final int INDEX_SCAN = 3;
    public static final int INDEX_FILM = 4;


    private static final int TAG_HOME = R.string.tag_home;
    private static final int TAG_FILMS = R.string.tag_products;
    private static final int TAG_SCAN = R.string.tag_scan;
    private static final int TAG_CREATE_PRODUCT = R.string.tag_create_edit;

    private static final int TITLE_HOME = R.string.title_home;
    private static final int TITLE_FILMS = R.string.title_products;
    private static final int TITLE_FILM = R.string.title_product;
    private static final int TITLE_SCAN = R.string.title_scan;
    private static final int TITLE_CREATE_PRODUCT = R.string.title_create;

    public static int navItemIndex;
    private static NavigationView navigationView;
    private static Toolbar toolbar;
    private final Handler handler = new Handler();
    public int CURRENT_TAG;
    private DrawerLayout drawer;
    private int countBack = 0;

    public static void selectNavMenu(int newNavItemIndex) {
        navItemIndex = newNavItemIndex;
        selectNavMenu();
        showTitle();
    }

    private static void selectNavMenu() {
        for (int i = 0; i <= INDEX_SCAN; i++) {
            deselectNavMenu(i);
        }
        if (navItemIndex <= INDEX_SCAN)
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private static void deselectNavMenu(int position) {
        if (navItemIndex <= INDEX_SCAN)
            navigationView.getMenu().getItem(position).setChecked(false);
    }

    private static void showTitle() {
        switch (navItemIndex) {
            case INDEX_FILMS:
                // List FILMS
                toolbar.setTitle(TITLE_FILMS);
                return;
            case INDEX_FILM:
                // SCAN
                toolbar.setTitle(TITLE_FILM);
                return;
            case INDEX_SCAN:
                // SCAN
                toolbar.setTitle(TITLE_SCAN);
                return;
            case INDEX_CREATE_PRODUCT:
                // SCAN
                toolbar.setTitle(TITLE_CREATE_PRODUCT);
                return;
            case INDEX_HOME:
                // home
            default:
                toolbar.setTitle(TITLE_HOME);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        SQLiteHelper databaseHelper = OpenHelperManager.getHelper(this, SQLiteHelper.class);
        int idUser = SaveSharedPreference.getLoggedInUser(getApplicationContext());
        try {
            databaseHelper.getUserDao();
            if (databaseHelper.readUser(idUser) == null) {
                User user = new User();
                idUser = 0;
                user.setId(idUser);
                SaveSharedPreference.setLoggedInUser(getApplicationContext(), user);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        if (idUser == 0) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            navItemIndex = INDEX_HOME;
            CURRENT_TAG = TAG_HOME;
            loadFragment();
        }

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(drawer.getWindowToken(), 0);
                }
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 123);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_db) {
            Intent dbmanager = new Intent(this, AndroidDatabaseManager.class);
            startActivity(dbmanager);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.frame);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                if (countBack == 0) {
                    countBack++;
                    Toast.makeText(this, getString(R.string.action_back_main), Toast.LENGTH_LONG).show();
                    handler.postDelayed(() -> countBack = 0, 2000);
                } else {
                    finishAffinity();
                }
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        //Replacing the main content with ContentFragment Which is our Inbox View;
        if (id == R.id.nav_home) {
            navItemIndex = INDEX_HOME;
            CURRENT_TAG = TAG_HOME;
        } else if (id == R.id.nav_products) {
            navItemIndex = INDEX_FILMS;
            CURRENT_TAG = TAG_FILMS;
        } else if (id == R.id.nav_scan) {
            navItemIndex = INDEX_SCAN;
            CURRENT_TAG = TAG_SCAN;
        } else if (id == R.id.nav_create_product) {
            navItemIndex = INDEX_CREATE_PRODUCT;
            CURRENT_TAG = TAG_CREATE_PRODUCT;
        }

        item.setChecked(!item.isChecked());
        item.setChecked(true);

        loadFragment();
        return true;
    }

    private void loadFragment() {

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            if (getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size() - 1).getTag().compareTo(getString(CURRENT_TAG)) == 0) {
                drawer.closeDrawers();
                return;
            }
        }

        Runnable mPendingRunnable = () -> {
            // update the main content by replacing fragments
            Fragment fragment = getFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment, getString(CURRENT_TAG)).addToBackStack(getString(CURRENT_TAG));//sync to fix
            fragmentTransaction.commitAllowingStateLoss();
        };
        // If mPendingRunnable is not null, then add to the message queue
        handler.post(mPendingRunnable);

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getFragment() {
        switch (navItemIndex) {
            case INDEX_FILMS:
                // List FILMS
                return new ListProductFragment();
            case INDEX_SCAN:
                // SCAN
                return new ScanFragment();
            case INDEX_CREATE_PRODUCT:
                // SCAN
                return new CreateEditProductFragment();
            case INDEX_HOME:
                // home
            default:
                return new HomeFragment();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }
}