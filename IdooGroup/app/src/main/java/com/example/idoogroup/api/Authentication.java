package com.example.idoogroup.api;

import com.example.idoogroup.model.User;

public class Authentication {
    private String password;
    private boolean rememberMe;
    private String username;
    private String token;

    public Authentication(User user) {
        password = user.getPassword();
        rememberMe = true;
        username = user.getLogin();
        token = user.getToken();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
