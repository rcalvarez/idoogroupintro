package com.example.idoogroup.api;

public class ConstantRestApi {

    public static final String ROOT_URL = "http://192.241.223.134:8080/ueg/api/";

    //KEYS
    public static final String KEY_GET_AUTHENTICATE = "authenticate";
    public static final String KEY_GET_ACCOUNT = "account";

    public static final String KEY_GET_PRODUCTS = "products";
    public static final String KEY_GET_PRODUCT = "products/{id}";

    public static final String KEY_GET_MANUFACTURERS = "manufacturers";

    public static final String KEY_GET_ITEMS = "items";
    public static final String KEY_GET_FILMS = "films";
    public static final String KEY_GET_FILM = "films/{id}";

    //URLS
    public static final String URL_GET_AUTHENTICATE = ROOT_URL + KEY_GET_AUTHENTICATE;
    public static final String URL_GET_ACCOUNT = ROOT_URL + KEY_GET_ACCOUNT;

    public static final String URL_GET_PRODUCTS = ROOT_URL + KEY_GET_PRODUCTS;
    public static final String URL_GET_PRODUCT = ROOT_URL + KEY_GET_PRODUCT;

    public static final String URL_GET_MANUFACTURERS = ROOT_URL + KEY_GET_MANUFACTURERS;

    public static final String URL_GET_FILMS = ROOT_URL + KEY_GET_FILMS;
    public static final String URL_GET_FILM = ROOT_URL + KEY_GET_FILM;
    public static final String URL_GET_ITEMS = ROOT_URL + KEY_GET_ITEMS;

}
