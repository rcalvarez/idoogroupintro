package com.example.idoogroup.api;

import com.example.idoogroup.api.response.AccountResponse;
import com.example.idoogroup.api.response.AuthResponse;
import com.example.idoogroup.api.response.FilmResponse;
import com.example.idoogroup.api.response.FilmsResponse;
import com.example.idoogroup.api.response.ItemsResponse;
import com.example.idoogroup.api.response.ManufacturersResponse;
import com.example.idoogroup.api.response.ProductResponse;
import com.example.idoogroup.api.response.ProductsResponse;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EndpointsApi {
    //ACCOUNT
    @Headers({"Accept: */*", "Content-Type: application/json"})
    @POST(ConstantRestApi.URL_GET_AUTHENTICATE)
    Call<AuthResponse> authenticate(@Body JsonElement jsonElement);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_ACCOUNT)
    Call<AccountResponse> getAccount();

    //PRODUCTS
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_PRODUCTS)
    Call<ProductsResponse> getProducts();

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_PRODUCT)
    Call<ProductResponse> getProduct(@Path("id") int id);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST(ConstantRestApi.URL_GET_PRODUCTS)
    Call<ProductResponse> createProduct(@Body JsonElement jsonElement);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @PUT(ConstantRestApi.URL_GET_PRODUCTS)
    Call<ProductResponse> updateProduct(@Body JsonElement jsonElement);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @DELETE(ConstantRestApi.URL_GET_PRODUCT)
    Call<ProductResponse> deleteProduct(@Path("id") int id);

    //MANUFACTURERS
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_MANUFACTURERS)
    Call<ManufacturersResponse> getManufacturers();

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_ITEMS)
    Call<ItemsResponse> getItems();

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_FILMS)
    Call<FilmsResponse> getFilms();

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET(ConstantRestApi.URL_GET_FILM)
    Call<FilmResponse> getFilm(@Path("id") String id);
}
