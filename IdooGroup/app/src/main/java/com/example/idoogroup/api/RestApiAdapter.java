package com.example.idoogroup.api;

import com.example.idoogroup.api.desearilizer.ProductDeserializer;
import com.example.idoogroup.api.desearilizer.ProductsDeserializer;
import com.example.idoogroup.api.response.ProductResponse;
import com.example.idoogroup.api.response.ProductsResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApiAdapter {
    public EndpointsApi setUpConnectionRestApi(Gson gson, OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ConstantRestApi.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(EndpointsApi.class);
    }

    public Gson convertGsonDeserializerProducts() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ProductsResponse.class, new ProductsDeserializer());

        return gsonBuilder.create();
    }

    public Gson convertGsonDeserializerProduct() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ProductResponse.class, new ProductDeserializer());

        return gsonBuilder.create();
    }

}
