package com.example.idoogroup.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    private String id_token;

    public TokenInterceptor(String id_token) {
        this.id_token = id_token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder()
                .header("Authorization", "Bearer " + id_token).build();
        return chain.proceed(newRequest);
    }

}
