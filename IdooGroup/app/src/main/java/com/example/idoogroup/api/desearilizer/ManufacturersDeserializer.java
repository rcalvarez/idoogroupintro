package com.example.idoogroup.api.desearilizer;

import com.example.idoogroup.api.response.ManufacturersResponse;
import com.example.idoogroup.model.Manufactured;
import com.example.idoogroup.utils.DateUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ManufacturersDeserializer implements JsonDeserializer<ManufacturersResponse> {

    @Override
    public ManufacturersResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray manufacturersResponseData = json.getAsJsonArray();
        ManufacturersResponse manufacturersResponse = new ManufacturersResponse();
        manufacturersResponse.setManufacturers(deserializerManufacturers(manufacturersResponseData));
        return manufacturersResponse;
    }

    private ArrayList<Manufactured> deserializerManufacturers(JsonArray manufacturersResponseData) {
        ArrayList<Manufactured> manufacturers = new ArrayList<>();

        for (int i = 0; i < manufacturersResponseData.size(); i++) {
            JsonObject jsonObject = manufacturersResponseData.get(i).getAsJsonObject();

            Manufactured manufactured = new Manufactured();

            manufactured.setName(jsonObject.get("name").getAsString());
            manufactured.setActive(jsonObject.get("active").getAsBoolean());
            manufactured.setCreateDate(DateUtils.ApiDateToDate(jsonObject.get("createDate").getAsString()));
            manufactured.setUpdateDate(DateUtils.ApiDateToDate(jsonObject.get("updateDate").getAsString()));

//            StringBuilder peoples = new StringBuilder();
//            for (int j = 0; j < jsonObject.get("people").getAsJsonArray().size(); j++) {
//                peoples.append(jsonObject.get("people").getAsJsonArray().get(j).getAsString());
//                if (j != jsonObject.get("people").getAsJsonArray().size() - 1)
//                    peoples.append(",");
//            }

//            manufactured.setPeople(peoples.toString());

//            StringBuilder species = new StringBuilder();
//            for (int j = 0; j < jsonObject.get("species").getAsJsonArray().size(); j++) {
//                species.append(jsonObject.get("species").getAsJsonArray().get(j).getAsString());
//                if (j != jsonObject.get("species").getAsJsonArray().size() - 1)
//                    species.append(",");
//            }
//
//            manufactured.setSpecies(species.toString());
//
//            StringBuilder locations = new StringBuilder();
//            for (int j = 0; j < jsonObject.get("locations").getAsJsonArray().size(); j++) {
//                locations.append(jsonObject.get("locations").getAsJsonArray().get(j).getAsString());
//                if (j != jsonObject.get("locations").getAsJsonArray().size() - 1)
//                    locations.append(",");
//            }
//
//            manufactured.setLocations(locations.toString());
//
//            StringBuilder vehicles = new StringBuilder();
//            for (int j = 0; j < jsonObject.get("vehicles").getAsJsonArray().size(); j++) {
//                vehicles.append(jsonObject.get("vehicles").getAsJsonArray().get(j).getAsString());
//                if (j != jsonObject.get("vehicles").getAsJsonArray().size() - 1)
//                    vehicles.append(",");
//            }
//            manufactured.setVehicles(vehicles.toString());
//
//            manufactured.setUrl(jsonObject.get("url").getAsString());

            manufacturers.add(manufactured);
        }

        return manufacturers;

    }
}
