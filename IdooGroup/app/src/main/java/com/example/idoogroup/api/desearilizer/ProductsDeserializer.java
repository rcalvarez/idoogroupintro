package com.example.idoogroup.api.desearilizer;

import com.example.idoogroup.api.response.ProductsResponse;
import com.example.idoogroup.model.Product;
import com.example.idoogroup.utils.DateUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ProductsDeserializer implements JsonDeserializer<ProductsResponse> {

    @Override
    public ProductsResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray productsResponseData = json.getAsJsonArray();
        ProductsResponse productsResponse = new ProductsResponse();
        productsResponse.setProducts(deserializerProducts(productsResponseData));
        return productsResponse;
    }

    private ArrayList<Product> deserializerProducts(JsonArray productsResponseData) {
        ArrayList<Product> products = new ArrayList<>();

        for (int i = 0; i < productsResponseData.size(); i++) {
            JsonObject jsonObject = productsResponseData.get(i).getAsJsonObject();

            Product product = new Product();

            product.setId(jsonObject.get("id").getAsInt());
            product.setInventoryId(jsonObject.get("inventaryID").getAsString());
            product.setActive(jsonObject.get("active").getAsBoolean());
            if (!(jsonObject.get("serialNumber") instanceof JsonNull)) {
                product.setSerialNumber(jsonObject.get("serialNumber").getAsString());
            } else {
                product.setSerialNumber("");
            }
            if (!(jsonObject.get("description") instanceof JsonNull)) {
                product.setDescription(jsonObject.get("description").getAsString());
            } else {
                product.setDescription("");
            }
            if (!(jsonObject.get("latitude") instanceof JsonNull)) {
                product.setLatitude(jsonObject.get("latitude").getAsString());
            } else {
                product.setLatitude("");
            }
            if (!(jsonObject.get("longitude") instanceof JsonNull)) {
                product.setLongitude(jsonObject.get("longitude").getAsString());
            } else {
                product.setLongitude("");
            }
            if (!(jsonObject.get("technicalSpecifications") instanceof JsonNull)) {
                product.setTechnicalSpecifications(jsonObject.get("technicalSpecifications").getAsString());
            } else {
                product.setTechnicalSpecifications("");
            }
            if (!(jsonObject.get("weight") instanceof JsonNull)) {
                product.setWeight(jsonObject.get("weight").getAsDouble());
            } else {
                product.setWeight(0.0);
            }
            if (!(jsonObject.get("tariffFraction") instanceof JsonNull)) {
                product.setTariffFraction(jsonObject.get("tariffFraction").getAsDouble());
            } else {
                product.setTariffFraction(0.0);
            }
            if (!(jsonObject.get("imageItem") instanceof JsonNull)) {
                product.setImageItem(jsonObject.get("imageItem").getAsString());
            } else {
                product.setImageItem("");
            }
            if (!(jsonObject.get("createDate") instanceof JsonNull)) {
                product.setCreateDate(DateUtils.ApiDateToDate(jsonObject.get("createDate").getAsString()));
            }
            if (!(jsonObject.get("modifiedDate") instanceof JsonNull)) {
                product.setUpdateDate(DateUtils.ApiDateToDate(jsonObject.get("modifiedDate").getAsString()));
            }

            products.add(product);
        }

        return products;

    }
}
