package com.example.idoogroup.api.response;

import com.example.idoogroup.model.User;
import com.example.idoogroup.utils.DateUtils;
import com.google.gson.annotations.SerializedName;

public class AccountResponse {

    private User user;

    @SerializedName("id")
    private int id;
    @SerializedName("login")
    private String login;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("email")
    private String email;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("activated")
    private boolean activated;
    @SerializedName("langKey")
    private String langKey;
    @SerializedName("createdBy")
    private String createdBy;
    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("lastModifiedBy")
    private String lastModifiedBy;
    @SerializedName("lastModifiedDate")
    private String lastModifiedDate;

    public AccountResponse() {
        user = new User();
    }

    public AccountResponse getAccount(User user) {
        this.user = user;
        return this;
    }

    public void setUser(AccountResponse user) {
        this.user = user.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUserEntity() {
        user.setId(id);
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmails(email);
        user.setImageUrl(imageUrl);
        user.setActivated(activated);
        user.setLandKey(langKey);
        user.setCreateBy(createdBy);
        user.setCreateDate(DateUtils.ApiDateToDate(createdDate));
        user.setLastModifiedBy(lastModifiedBy);
        user.setLastModifiedDate(DateUtils.ApiDateToDate(lastModifiedDate));
        return user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

}
