package com.example.idoogroup.api.response;

import com.example.idoogroup.model.Film;
import com.google.gson.annotations.SerializedName;

public class FilmResponse {

    private Film film;

    @SerializedName("film_id")
    private String film_id;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("director")
    private String director;
    @SerializedName("producer")
    private String producer;
    @SerializedName("release_date")
    private int release_date;
    @SerializedName("rt_score")
    private int rt_score;
    @SerializedName("people")
    private String people;
    @SerializedName("species")
    private String species;
    @SerializedName("locations")
    private String locations;
    @SerializedName("vehicles")
    private String vehicles;
    @SerializedName("url")
    private String url;

    public FilmResponse() {
        film = new Film();
    }

    public FilmResponse getFilm() {
        return this;
    }

    public void setFilm(FilmResponse film) {
        this.film = film.film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Film getFilmEntity() {
        return film;
    }
}