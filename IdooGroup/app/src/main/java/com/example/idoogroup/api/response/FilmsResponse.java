package com.example.idoogroup.api.response;

import com.example.idoogroup.model.Film;

import java.util.ArrayList;

public class FilmsResponse {
    ArrayList<Film> films;

    public FilmsResponse() {
    }

    public ArrayList<Film> getFilms() {
        return films;
    }

    public void setFilms(ArrayList<Film> films) {
        this.films = films;
    }
}
