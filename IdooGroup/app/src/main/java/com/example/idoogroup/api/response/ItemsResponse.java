package com.example.idoogroup.api.response;

import com.example.idoogroup.model.Item;

import java.util.ArrayList;

public class ItemsResponse {
    ArrayList<Item> items;

    public ItemsResponse() {
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

//    @Override
//    public String toString() {
//        return "ItemsResponse{" +
//                "Title items=" + items.get(0).getTitle() +
//                '}';
//    }
}
