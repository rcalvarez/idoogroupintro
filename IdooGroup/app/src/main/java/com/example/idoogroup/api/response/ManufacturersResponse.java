package com.example.idoogroup.api.response;

import com.example.idoogroup.model.Manufactured;

import java.util.ArrayList;

public class ManufacturersResponse {
    ArrayList<Manufactured> manufacturers;

    public ManufacturersResponse() {
    }

    public ArrayList<Manufactured> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(ArrayList<Manufactured> manufacturers) {
        this.manufacturers = manufacturers;
    }
}
