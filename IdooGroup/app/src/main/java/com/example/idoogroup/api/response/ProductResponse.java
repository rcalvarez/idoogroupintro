package com.example.idoogroup.api.response;

import com.example.idoogroup.model.Product;
import com.google.gson.annotations.SerializedName;

public class ProductResponse {

    private Product product;

    @SerializedName("id")
    private int id;
    @SerializedName("inventaryID")
    private String inventoryID;
    @SerializedName("serialNumber")
    private String serialNumber;
    @SerializedName("description")
    private String description;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("technicalSpecifications")
    private String technicalSpecifications;
    @SerializedName("weight")
    private double weight;
    @SerializedName("tariffFraction")
    private double tariffFraction;
    @SerializedName("imageItem")
    private String imageItem;
    @SerializedName("active")
    private boolean active;
    @SerializedName("createDate")
    private String createDate;
    @SerializedName("modifiedDate")
    private String modifiedDate;

    public ProductResponse() {
        product = new Product();
    }

    public ProductResponse getProduct() {
        return this;
    }

    public void setProduct(ProductResponse productResponse) {
        this.product = productResponse.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProductEntity() {
        return product;
    }
}