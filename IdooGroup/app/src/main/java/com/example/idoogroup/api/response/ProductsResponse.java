package com.example.idoogroup.api.response;

import com.example.idoogroup.model.Product;

import java.util.ArrayList;

public class ProductsResponse {
    ArrayList<Product> products;

    public ProductsResponse() {
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
