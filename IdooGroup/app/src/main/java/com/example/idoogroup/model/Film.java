package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.jetbrains.annotations.NotNull;

@DatabaseTable(tableName = "Film")
public class Film {
    @DatabaseField(generatedId = true)
    protected int id;
    @DatabaseField(dataType = DataType.STRING, unique = true)
    private String film_id;
    @DatabaseField(dataType = DataType.STRING)
    private String title;
    @DatabaseField(dataType = DataType.STRING)
    private String description;
    @DatabaseField(dataType = DataType.STRING)
    private String director;
    @DatabaseField(dataType = DataType.STRING)
    private String producer;
    @DatabaseField(dataType = DataType.INTEGER)
    private int release_date;
    @DatabaseField(dataType = DataType.INTEGER)
    private int rt_score;
    @DatabaseField(dataType = DataType.STRING)
    private String people;
    @DatabaseField(dataType = DataType.STRING)
    private String species;
    @DatabaseField(dataType = DataType.STRING)
    private String locations;
    @DatabaseField(dataType = DataType.STRING)
    private String vehicles;
    @DatabaseField(dataType = DataType.STRING)
    private String url;
    @DatabaseField(dataType = DataType.INTEGER)
    private int number_shared;

    public Film() {
        number_shared = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilm_id() {
        return film_id;
    }

    public void setFilm_id(String film_id) {
        this.film_id = film_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getRelease_date() {
        return release_date;
    }

    public void setRelease_date(int release_date) {
        this.release_date = release_date;
    }

    public int getRt_score() {
        return rt_score;
    }

    public void setRt_score(int rt_score) {
        this.rt_score = rt_score;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getVehicles() {
        return vehicles;
    }

    public void setVehicles(String vehicles) {
        this.vehicles = vehicles;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getNumber_shared() {
        return number_shared;
    }

    public void setNumber_shared(int number_shared) {
        this.number_shared = number_shared;
    }

    @NotNull
    @Override
    public String toString() {
        return "Film{" +
                "title='" + title + '\'' +
                ", director='" + director + '\'' +
                '}';
    }
}
