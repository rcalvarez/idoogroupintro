package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

abstract class General {
    //    @DatabaseField(generatedId = true)
    @DatabaseField(dataType = DataType.INTEGER, id = true)
    protected int id;
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    protected boolean active;
    @DatabaseField(dataType = DataType.DATE)
    protected Date createDate;
    @DatabaseField(dataType = DataType.DATE)
    protected Date updateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
