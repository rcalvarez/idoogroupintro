package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.jetbrains.annotations.NotNull;

@DatabaseTable(tableName = "Item")
public class Item {
    @DatabaseField(generatedId = true)
    protected int id;
    @DatabaseField(dataType = DataType.STRING)
    private String name;
    @DatabaseField(dataType = DataType.STRING)
    private String description;
    @DatabaseField(dataType = DataType.STRING)
    private String reference;
    @DatabaseField(dataType = DataType.STRING)
    private String status;
    @DatabaseField(dataType = DataType.STRING)
    private String itemType;
    @DatabaseField(dataType = DataType.STRING)
    private String tax;
    @DatabaseField(dataType = DataType.STRING)
    private String type;

//    @DatabaseField(canBeNull = false, foreign = true)
//    private Categ categ;

//    @DatabaseField(canBeNull = false, foreign = true)
//    private Invent invent;

//    @DatabaseField(canBeNull = false, foreign = true)
//    private ItemCateg itemCateg;

//    @ForeignCollectionField(eager = false, orderColumnName = "price", orderAscending = false)
//    private ForeignCollection<Price> prices; //N-N

//    @ForeignCollectionField(eager = false, orderColumnName = "percentage", orderAscending = false)
//    private ForeignCollection<Tax> taxes; //N-N

//    private Categ tmpCateg;
//    private Invent tmpInvent;
//    private List<Price> tmpPrices;
//    private List<Tax> tmpTaxes;

    public Item() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

//    public Categ getCateg() {
//        return categ;
//    }
//
//    public void setCateg(Categ categ) {
//        this.categ = categ;
//    }
//
//    public Invent getInvent() {
//        return invent;
//    }

//    public void setInvent(Invent invent) {
//        this.invent = invent;
//    }
//
//    public ItemCateg getItemCateg() {
//        return itemCateg;
//    }
//
//    public void setItemCateg(ItemCateg itemCateg) {
//        this.itemCateg = itemCateg;
//    }
//
//    public ForeignCollection<Price> getPrices() {
//        return prices;
//    }
//
//    public void setPrices(ForeignCollection<Price> prices) {
//        this.prices = prices;
//    }
//
//    public ForeignCollection<Tax> getTaxes() {
//        return taxes;
//    }
//
//    public void setTaxes(ForeignCollection<Tax> taxes) {
//        this.taxes = taxes;
//    }
//
//    public Categ getTmpCateg() {
//        return tmpCateg;
//    }
//
//    public void setTmpCateg(Categ tmpCateg) {
//        this.tmpCateg = tmpCateg;
//    }
//
//    public Invent getTmpInvent() {
//        return tmpInvent;
//    }
//
//    public void setTmpInvent(Invent tmpInvent) {
//        this.tmpInvent = tmpInvent;
//    }
//
//    public List<Price> getTmpPrices() {
//        return tmpPrices;
//    }
//
//    public void setTmpPrices(List<Price> tmpPrices) {
//        this.tmpPrices = tmpPrices;
//    }
//
//    public List<Tax> getTmpTaxes() {
//        return tmpTaxes;
//    }
//
//    public void setTmpTaxes(List<Tax> tmpTaxes) {
//        this.tmpTaxes = tmpTaxes;
//    }

    @NotNull
    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
