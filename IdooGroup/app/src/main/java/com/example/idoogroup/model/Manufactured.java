package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Manufactured")
public class Manufactured extends General {
    //    @DatabaseField(generatedId = true)
    @DatabaseField(dataType = DataType.STRING)
    private String name;

    public Manufactured() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
