package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.jetbrains.annotations.NotNull;

@DatabaseTable(tableName = "Product")
public class Product extends General {
    //    @DatabaseField(generatedId = true)
    @DatabaseField(dataType = DataType.STRING)
    private String inventoryId;
    @DatabaseField(dataType = DataType.STRING)
    private String serialNumber;
    @DatabaseField(dataType = DataType.STRING)
    private String description;
    @DatabaseField(dataType = DataType.STRING)
    private String latitude;
    @DatabaseField(dataType = DataType.STRING)
    private String longitude;
    @DatabaseField(dataType = DataType.STRING)
    private String technicalSpecifications;
    @DatabaseField(dataType = DataType.DOUBLE)
    private double weight;
    @DatabaseField(dataType = DataType.DOUBLE)
    private double tariffFraction;
    @DatabaseField(dataType = DataType.STRING)
    private String imageItem;

    @DatabaseField(foreign = true)
    private Manufactured manufactured;

    private Manufactured tmpManufactured;

    public Product() {
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTechnicalSpecifications() {
        return technicalSpecifications;
    }

    public void setTechnicalSpecifications(String technicalSpecifications) {
        this.technicalSpecifications = technicalSpecifications;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getTariffFraction() {
        return tariffFraction;
    }

    public void setTariffFraction(double tariffFraction) {
        this.tariffFraction = tariffFraction;
    }

    public String getImageItem() {
        return imageItem;
    }

    public void setImageItem(String imageItem) {
        this.imageItem = imageItem;
    }

    public Manufactured getManufactured() {
        return manufactured;
    }

    public void setManufactured(Manufactured manufactured) {
        this.manufactured = manufactured;
    }

    public Manufactured getTmpManufactured() {
        return tmpManufactured;
    }

    public void setTmpManufactured(Manufactured tmpManufactured) {
        this.tmpManufactured = tmpManufactured;
    }

    @NotNull

    @Override
    public String toString() {
        return "Product{" +
                "serialNumber='" + serialNumber + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
