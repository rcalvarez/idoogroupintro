package com.example.idoogroup.model;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.idoogroup.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class SQLiteHelper extends OrmLiteSqliteOpenHelper {
    // Database Info
    private static final String DATABASE_NAME = "idooGroup.db";
    private static final int DATABASE_VERSION = 10;
    private static SQLiteHelper sInstance; //singleton
    // DAO's
    private Dao<Manufactured, Integer> manufacturedDao;
    private Dao<Product, Integer> productDao;
    private Dao<UnitMeasure, Integer> unitMeasureDao;
    private Dao<User, Integer> userDao;
    private Dao<UserExtra, Integer> userExtraDao;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    //se ejecuta sincronamente (analizar si usarla)
    public static synchronized SQLiteHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SQLiteHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Manufactured.class);
            TableUtils.createTable(connectionSource, Product.class);
            TableUtils.createTable(connectionSource, UnitMeasure.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, UserExtra.class);
        } catch (SQLException e) {
            Log.e(SQLiteHelper.class.getName(), "Unable to create databases", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {
            TableUtils.dropTable(connectionSource, Manufactured.class, true);
            TableUtils.dropTable(connectionSource, Product.class, true);
            TableUtils.dropTable(connectionSource, UnitMeasure.class, true);
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, UserExtra.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(SQLiteHelper.class.getName(), "Unable to upgrade database from version " + i + " to new "
                    + i1, e);
        }
    }

    //get DAO's
    public Dao<Manufactured, Integer> getManufacturedDao() throws SQLException {
        if (manufacturedDao == null) {
            manufacturedDao = getDao(Manufactured.class);
        }
        return manufacturedDao;
    }

    public Dao<Product, Integer> getProductDao() throws SQLException {
        if (productDao == null) {
            productDao = getDao(Product.class);
        }
        return productDao;
    }

    public Dao<UnitMeasure, Integer> getUnitMeasureDao() throws SQLException {
        if (unitMeasureDao == null) {
            unitMeasureDao = getDao(UnitMeasure.class);
        }
        return unitMeasureDao;
    }

    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }

    public Dao<UserExtra, Integer> getUserExtraDao() throws SQLException {
        if (userExtraDao == null) {
            userExtraDao = getDao(UserExtra.class);
        }
        return userExtraDao;
    }

    //CRUD OPERATIONS OVER MANUFACTURED
    public int createManufactured(final Manufactured manufactured) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return manufacturedDao.create(manufactured);
                });
    }


    public Dao.CreateOrUpdateStatus createOrUpdateManufactured(final Manufactured manufactured) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return manufacturedDao.createOrUpdate(manufactured);
                    // you could pass back an object here
//                        return null;
                });
    }

    public int updateManufactured(final Manufactured manufactured) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> manufacturedDao.update(manufactured));
    }

    public int deleteManufactured(final Manufactured manufactured) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return manufacturedDao.delete(manufactured);
                    // you could pass back an object here
//                        return null;
                });
    }

    //to watch place's info
    public Manufactured readManufactured(final int id) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> manufacturedDao.queryForId(id));
    }

    public List<Manufactured> readAllManufactured() throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> manufacturedDao.queryForAll());
    }

    //CRUD OPERATIONS OVER PRODUCT
    public int createProduct(final Product product) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return productDao.create(product);
                });
    }

    public Dao.CreateOrUpdateStatus createOrUpdateProduct(final Product product) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return productDao.createOrUpdate(product);
                    // you could pass back an object here
//                        return null;
                });
    }

    public int updateProduct(final Product product) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> productDao.update(product));
    }

    public int deleteProduct(final Product product) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return productDao.delete(product);
                    // you could pass back an object here
//                        return null;
                });
    }

    //to watch place's info
    public Product readProduct(final int id) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> productDao.queryForId(id));
    }

    public List<Product> readAllProduct() throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> productDao.queryForAll());
    }

    public List<Product> readSimilarProducts(Product product) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    QueryBuilder<Product, Integer> products = productDao.queryBuilder();
                    products.where().eq("inventoryId", product.getInventoryId());

                    List<Product> result = products.query();
                    if (result.size() > 6)
                        return result.subList(0, 6);
                    else
                        return result;
                });
    }

//    public int createItem(final Item item) throws SQLException {
//        // we need the final to see it within the Callable
//        return TransactionManager.callInTransaction(connectionSource,
//                () -> {
//                    // insert our order
//                    Categ categ = item.getTmpCateg();
//                    categoryDao.createIfNotExists(categ);
//                    categoryDao.refresh(categ);
//                    item.setCateg(categ);
//
//                    ItemCateg itemCateg = item.getTmpItemCateg();
//                    itemCategoryDao.createIfNotExists(itemCateg);
//                    itemCategoryDao.refresh(itemCateg);
//                    item.setItemCateg(itemCateg);
//
//                    if (item.getTmpInvent() != null) {
//                        Invent invent = item.getTmpInvent();
//                        inventoryDao.createIfNotExists(invent);
//                        inventoryDao.refresh(invent);
//                        item.setInvent(invent);
//                    }
//
//                    Item tmpItem = itemDao.createIfNotExists(item);
//                    itemDao.refresh(tmpItem);
//
//                    for (Tax t :
//                            tmpItem.getTmpTaxes()) {
//                        t.setItem(tmpItem);
//                        taxDao.createIfNotExists(t);
//                    }
//
//                    for (Price p :
//                            tmpItem.getTmpPrices()) {
//                        p.setItem(tmpItem);
//                        priceDao.createIfNotExists(p);
//                    }
//
//                    return tmpItem.getId();
//                });
//    }


    //CRUD OPERATIONS OVER UNIT MEASURE
    public int createUnitMeasure(final UnitMeasure unitMeasure) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return unitMeasureDao.create(unitMeasure);
                });
    }

    public Dao.CreateOrUpdateStatus createOrUpdateUnitMeasure(final UnitMeasure unitMeasure) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return unitMeasureDao.createOrUpdate(unitMeasure);
                    // you could pass back an object here
//                        return null;
                });
    }

    public int updateUnitMeasure(final UnitMeasure unitMeasure) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> unitMeasureDao.update(unitMeasure));
    }

    public int deleteUnitMeasure(final UnitMeasure unitMeasure) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return unitMeasureDao.delete(unitMeasure);
                    // you could pass back an object here
//                        return null;
                });
    }

    //to watch place's info
    public UnitMeasure readUnitMeasure(final int id) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> unitMeasureDao.queryForId(id));
    }

    public List<UnitMeasure> readAllUnitMeasure() throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> unitMeasureDao.queryForAll());
    }

    //CRUD OPERATIONS OVER USER
    public int createUser(final User user) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    userDao.createIfNotExists(user);
                    userDao.refresh(user);
                    return user.getId();
                });
    }


    public Dao.CreateOrUpdateStatus createOrUpdateUser(final User user) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return userDao.createOrUpdate(user);
                    // you could pass back an object here
//                        return null;
                });
    }

    public int updateUser(final User user) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> userDao.update(user));
    }

    public int deleteUser(final User user) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return userDao.delete(user);
                    // you could pass back an object here
//                        return null;
                });
    }

    //to watch place's info
    public User readUser(final int id) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> userDao.queryForId(id));
    }

    public List<User> readAllUser() throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> userDao.queryForAll());
    }

    //CRUD OPERATIONS OVER USER EXTRA
    public int createUserExtra(final UserExtra userExtra) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return userExtraDao.create(userExtra);
                });
    }


    public Dao.CreateOrUpdateStatus createOrUpdateUserExtra(final UserExtra userExtra) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return userExtraDao.createOrUpdate(userExtra);
                    // you could pass back an object here
//                        return null;
                });
    }

    public int updateUserExtra(final UserExtra userExtra) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> userExtraDao.update(userExtra));
    }

    public int deleteUserExtra(final UserExtra userExtra) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> {
                    // insert our order
                    return userExtraDao.delete(userExtra);
                    // you could pass back an object here
//                        return null;
                });
    }

    //to watch place's info
    public UserExtra readUserExtra(final int id) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> userExtraDao.queryForId(id));
    }

    public List<UserExtra> readAllUserExtra() throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                () -> userExtraDao.queryForAll());
    }

//    -----------------------------------------------------------------------------------------------

    // this method is for using Database Manager Library
    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"message"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try {
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(Query, null);

            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
//        } catch(SQLException sqlEx){
//            Log.d("printing exception", sqlEx.getMessage());
//            //if any exceptions are triggered save the error message to cursor an return the arraylist
//            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
//            alc.set(1,Cursor2);
//            return alc;
        } catch (Exception ex) {
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }

// Iterator example (more efficient than usual for loops)
//    CloseableIterator<Account> iterator = accountDao.closeableIterator();
//    try {
//            while (iterator.hasNext()) {
//                Account account = iterator.next();
//                System.out.println(account.getName());
//            }
//        } finally
//
//        {
//    // close it at the end to close underlying SQL statement
//            iterator.close();
//        }


}
