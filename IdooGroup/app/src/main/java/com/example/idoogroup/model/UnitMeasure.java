package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "UnitMeasure")
public class UnitMeasure extends General {
    //    @DatabaseField(generatedId = true)
    @DatabaseField(dataType = DataType.STRING)
    private String name;

    public UnitMeasure() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
