package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

@DatabaseTable(tableName = "User")
public class User {
    @DatabaseField(dataType = DataType.INTEGER, id = true)
    protected int id;
    @DatabaseField(dataType = DataType.STRING, unique = true, canBeNull = false)
    private String login;
    @DatabaseField(dataType = DataType.STRING)
    private String firstName;
    @DatabaseField(dataType = DataType.STRING)
    private String lastName;
    @DatabaseField(dataType = DataType.STRING, canBeNull = false)
    private String emails;
    @DatabaseField(dataType = DataType.BOOLEAN, canBeNull = false)
    private boolean activated;
    @DatabaseField(dataType = DataType.STRING)
    private String imageUrl;
    @DatabaseField(dataType = DataType.STRING)
    private String landKey;
    @DatabaseField(dataType = DataType.STRING)
    private String createBy;
    @DatabaseField(dataType = DataType.DATE)
    private Date createDate;
    @DatabaseField(dataType = DataType.STRING)
    private String lastModifiedBy;
    @DatabaseField(dataType = DataType.DATE)
    private Date lastModifiedDate;
    @DatabaseField(dataType = DataType.STRING)
    private String password;
    @DatabaseField(dataType = DataType.STRING)
    private String token;

    @DatabaseField(foreign = true)
    private UserExtra userExtra;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLandKey() {
        return landKey;
    }

    public void setLandKey(String landKey) {
        this.landKey = landKey;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @NotNull
    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
