package com.example.idoogroup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "UserExtra")
public class UserExtra {
    @DatabaseField(dataType = DataType.INTEGER, id = true)
    protected int id;
    @DatabaseField(dataType = DataType.STRING)
    protected String gender;
    @DatabaseField(dataType = DataType.STRING)
    protected String phoneHome;
    @DatabaseField(dataType = DataType.STRING)
    protected String phoneMobil;
    @DatabaseField(dataType = DataType.DATE)
    protected Date lastAccess;

    public UserExtra() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    public String getPhoneMobil() {
        return phoneMobil;
    }

    public void setPhoneMobil(String phoneMobil) {
        this.phoneMobil = phoneMobil;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }
}
