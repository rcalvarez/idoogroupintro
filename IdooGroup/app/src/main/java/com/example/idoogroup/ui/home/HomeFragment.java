package com.example.idoogroup.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterViewFlipper;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.idoogroup.MainActivity;
import com.example.idoogroup.R;
import com.example.idoogroup.ui.product.createEdit.CreateEditProductFragment;
import com.example.idoogroup.ui.product.list.ListProductFragment;
import com.example.idoogroup.ui.scan.ScanFragment;

public class HomeFragment extends Fragment {

    private static final Handler handler = new Handler();
    private static final int[] TEXT = {R.string.flipper_text1, R.string.flipper_text2, R.string.flipper_text3, R.string.flipper_text4, R.string.flipper_text5};
    private static final int[] IMG = {R.drawable.seis, R.drawable.siete, R.drawable.tres, R.drawable.ocho, R.drawable.cinco};
    private AdapterViewFlipper flipper;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        TextView products = root.findViewById(R.id.products);
        TextView createProduct = root.findViewById(R.id.create_product);
        TextView scan = root.findViewById(R.id.scan);
        flipper = root.findViewById(R.id.flipper);

        FlipperAdapter adapter = new FlipperAdapter(getContext(), TEXT, IMG);
        flipper.setAdapter(adapter);

        products.setOnClickListener(v -> {
            Runnable mPendingRunnable = () -> {

                Fragment fragment = new ListProductFragment();

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_products)).addToBackStack(getString(R.string.tag_products));//sync to fix
                fragmentTransaction.commitAllowingStateLoss();
            };

            // If mPendingRunnable is not null, then add to the message queue
            handler.post(mPendingRunnable);
        });

        createProduct.setOnClickListener(v -> {
            Runnable mPendingRunnable = () -> {

                Fragment fragment = new CreateEditProductFragment();

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_create_edit)).addToBackStack(getString(R.string.tag_create_edit));//sync to fix
                fragmentTransaction.commitAllowingStateLoss();
            };

            // If mPendingRunnable is not null, then add to the message queue
            handler.post(mPendingRunnable);
        });

        scan.setOnClickListener(v -> {
            Runnable mPendingRunnable = () -> {

                Fragment fragment = new ScanFragment();

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_scan)).addToBackStack(getString(R.string.tag_scan));//sync to fix
                fragmentTransaction.commitAllowingStateLoss();
            };

            // If mPendingRunnable is not null, then add to the message queue
            handler.post(mPendingRunnable);
        });

        return root;
    }

    @Override
    public void onResume() {
        MainActivity.selectNavMenu(MainActivity.INDEX_HOME);
        super.onResume();
    }

    class FlipperAdapter extends BaseAdapter {
        Context ctx;
        int[] text;
        int[] img;
        LayoutInflater inflater;

        private FlipperAdapter(Context context, int[] text, int[] img) {
            ctx = context;
            this.text = text;
            this.img = img;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return text.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.flipper_items, null);
            TextView flipperText = convertView.findViewById(R.id.flipper_text);
            ImageView flipperImg = convertView.findViewById(R.id.flipper_img);
            flipperText.setText(text[position]);
            flipperImg.setImageResource(img[position]);

            ImageView first = convertView.findViewById(R.id.first);
            ImageView second = convertView.findViewById(R.id.second);
            ImageView third = convertView.findViewById(R.id.third);
            ImageView quarter = convertView.findViewById(R.id.quarter);
            ImageView fifth = convertView.findViewById(R.id.fifth);

            switch (position) {
                case 0:
                    first.setImageResource(R.drawable.circle_active);
                    second.setImageResource(R.drawable.circle_inactive);
                    third.setImageResource(R.drawable.circle_inactive);
                    quarter.setImageResource(R.drawable.circle_inactive);
                    fifth.setImageResource(R.drawable.circle_inactive);
                    break;
                case 1:
                    first.setImageResource(R.drawable.circle_inactive);
                    second.setImageResource(R.drawable.circle_active);
                    third.setImageResource(R.drawable.circle_inactive);
                    quarter.setImageResource(R.drawable.circle_inactive);
                    fifth.setImageResource(R.drawable.circle_inactive);
                    break;
                case 2:
                    first.setImageResource(R.drawable.circle_inactive);
                    second.setImageResource(R.drawable.circle_inactive);
                    third.setImageResource(R.drawable.circle_active);
                    quarter.setImageResource(R.drawable.circle_inactive);
                    fifth.setImageResource(R.drawable.circle_inactive);
                    break;
                case 3:
                    first.setImageResource(R.drawable.circle_inactive);
                    second.setImageResource(R.drawable.circle_inactive);
                    third.setImageResource(R.drawable.circle_inactive);
                    quarter.setImageResource(R.drawable.circle_active);
                    fifth.setImageResource(R.drawable.circle_inactive);
                    break;
                case 4:
                    first.setImageResource(R.drawable.circle_inactive);
                    second.setImageResource(R.drawable.circle_inactive);
                    third.setImageResource(R.drawable.circle_inactive);
                    quarter.setImageResource(R.drawable.circle_inactive);
                    fifth.setImageResource(R.drawable.circle_active);
                    break;
            }

            return convertView;
        }
    }
}