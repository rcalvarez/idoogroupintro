package com.example.idoogroup.ui.login;

import android.accounts.AccountAuthenticatorActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.idoogroup.MainActivity;
import com.example.idoogroup.R;
import com.example.idoogroup.api.Authentication;
import com.example.idoogroup.api.EndpointsApi;
import com.example.idoogroup.api.RestApiAdapter;
import com.example.idoogroup.api.TokenInterceptor;
import com.example.idoogroup.api.response.AccountResponse;
import com.example.idoogroup.api.response.AuthResponse;
import com.example.idoogroup.model.SQLiteHelper;
import com.example.idoogroup.model.User;
import com.example.idoogroup.utils.ProjectUtils;
import com.example.idoogroup.utils.SaveSharedPreference;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AccountAuthenticatorActivity {

    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";

    private final String TAG = this.getClass().getSimpleName();

    private SQLiteHelper databaseHelper;

    private Button signIn;
    private ProgressBar progressBarLoading;
    private TextView error;

    private EditText username;
    private EditText password;

    private User user;

    private AuthResponse authResponse;
    private AccountResponse accountResponse;
    private String userName;
    private String userPass;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        signIn = findViewById(R.id.sign_in);
        progressBarLoading = findViewById(R.id.progress_bar);
        error = findViewById(R.id.error);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        databaseHelper = OpenHelperManager.getHelper(getBaseContext(), SQLiteHelper.class);

        String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);

        if (accountName != null) {
            username.setText(accountName);
        }

        signIn.setOnClickListener(v -> {
            error.setVisibility(View.GONE);
            if (!ProjectUtils.isOnline(LoginActivity.this)) {
                ProjectUtils.offlineDialog(LoginActivity.this);
            } else if (username.getText().toString().equals("")) {
                username.setError(getString(R.string.required_value));
            } else if (password.getText().toString().equals("")) {
                password.setError(getString(R.string.required_value));
            } else {
                signIn.setVisibility(View.GONE);
                progressBarLoading.setVisibility(View.VISIBLE);
                submit();
            }
        });

    }

    public void submit() {

        OkHttpClient client = new OkHttpClient.Builder().build();
        RestApiAdapter restApiAdapter = new RestApiAdapter();
        Gson gson = restApiAdapter.convertGsonDeserializerProducts();

        EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

        userName = username.getText().toString();
        userPass = password.getText().toString();
        String body = "{ \"password\": \"" + userPass + "\", \"rememberMe\": true, \"username\": \"" + userName + "\" }";

        Call<AuthResponse> call = endpointsApi.authenticate(new JsonParser().parse(body));

        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(@NotNull Call<AuthResponse> call, @NotNull Response<AuthResponse> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, response.message());
                    error.setText(R.string.incorrect_credential);
                    error.setVisibility(View.VISIBLE);
                    signIn.setVisibility(View.VISIBLE);
                    progressBarLoading.setVisibility(View.GONE);
                    return;
                }

                authResponse = response.body();
                user = new User();
                user.setLogin(userName);
                user.setPassword(userPass);
                user.setToken(authResponse.getId_token());

                Toast.makeText(LoginActivity.this, "Authenticate", Toast.LENGTH_LONG).show();
                Log.i(TAG, "Authenticate");
                finishLogin();
            }

            @Override
            public void onFailure(@NotNull Call<AuthResponse> call, @NotNull Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

    }

    private void finishLogin() {
        Log.i(TAG, "Login SUCCESS");

        TokenInterceptor interceptor = new TokenInterceptor(new Authentication(user).getToken());
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        RestApiAdapter restApiAdapter = new RestApiAdapter();
        Gson gson = restApiAdapter.convertGsonDeserializerProducts();

        EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

        Call<AccountResponse> responseCall = endpointsApi.getAccount();

        responseCall.enqueue(new Callback<AccountResponse>() {
            @Override
            public void onResponse(@NotNull Call<AccountResponse> call, @NotNull Response<AccountResponse> response) {
                AccountResponse body = response.body();

                if (body != null) {
                    accountResponse = body.getAccount(user);
                }

                try {
                    login();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<AccountResponse> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_loads_user), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void login() throws SQLException {
        user = accountResponse.getUserEntity();
        databaseHelper.getUserDao();
        if (databaseHelper.readUser(user.getId()) == null)
            databaseHelper.createUser(user);
        else
            databaseHelper.updateUser(user);
        SaveSharedPreference.setLoggedInUser(getApplicationContext(), user);

        Log.i(TAG, "CREATE " + user + " User");

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}