package com.example.idoogroup.ui.product.createEdit;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.idoogroup.MainActivity;
import com.example.idoogroup.R;
import com.example.idoogroup.api.Authentication;
import com.example.idoogroup.api.EndpointsApi;
import com.example.idoogroup.api.RestApiAdapter;
import com.example.idoogroup.api.TokenInterceptor;
import com.example.idoogroup.api.response.ProductResponse;
import com.example.idoogroup.model.Product;
import com.example.idoogroup.model.SQLiteHelper;
import com.example.idoogroup.model.User;
import com.example.idoogroup.ui.product.details.DetailsProductFragment;
import com.example.idoogroup.utils.ProjectUtils;
import com.example.idoogroup.utils.SaveSharedPreference;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateEditProductFragment extends Fragment {

    private static final Handler handler = new Handler();
    private final String TAG = "CreateEditProductFrag";
    private int id;
    private String json;
    private Product product;
    private ProductResponse productResponse;
    private SQLiteHelper databaseHelper;

    private User user;

    private TextView error, serialNumber, inventoryId, latitude, longitude, weight, technicalSpecifications, tariffFraction, description;
    private RadioGroup activeRG;
    private RadioButton active, inactive;
    private Button save;
    private ProgressBar progressBarLoading;

    public CreateEditProductFragment(int id) {
        this.id = id;
    }

    public CreateEditProductFragment(String json) {
        this.json = json;
    }

    public CreateEditProductFragment(int id, String json) {
        this.id = id;
        this.json = json;
    }

    public CreateEditProductFragment() {

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_create_edit_product, container, false);

        databaseHelper = OpenHelperManager.getHelper(this.getContext(), SQLiteHelper.class);

        error = root.findViewById(R.id.error);
        serialNumber = root.findViewById(R.id.serial_number);
        activeRG = root.findViewById(R.id.active_radio_group);
        active = root.findViewById(R.id.active);
        inactive = root.findViewById(R.id.inactive);
        inventoryId = root.findViewById(R.id.inventory_id);
        latitude = root.findViewById(R.id.latitude);
        longitude = root.findViewById(R.id.longitude);
        weight = root.findViewById(R.id.weight);
        technicalSpecifications = root.findViewById(R.id.technical_specifications);
        tariffFraction = root.findViewById(R.id.tariff_fraction);
        description = root.findViewById(R.id.description);
        save = root.findViewById(R.id.save);
        progressBarLoading = root.findViewById(R.id.progress_bar);

        return root;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {

        MainActivity.selectNavMenu(MainActivity.INDEX_CREATE_PRODUCT);

        try {
            databaseHelper.getProductDao();
            databaseHelper.getUserDao();
            product = databaseHelper.readProduct(id);
            user = databaseHelper.readUser(SaveSharedPreference.getLoggedInUser(getActivity().getApplicationContext()));
            if (id != 0) {
                product = databaseHelper.readProduct(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (id != 0 && json == null) {
            serialNumber.setText(product.getSerialNumber());
            inventoryId.setText(product.getInventoryId());
            latitude.setText(product.getLatitude());
            longitude.setText(product.getLongitude());
            weight.setText(product.getWeight() + "");
            technicalSpecifications.setText(product.getTechnicalSpecifications());
            tariffFraction.setText(product.getTariffFraction() + "");
            description.setText(product.getDescription());
            active.setChecked(product.isActive());
            inactive.setChecked(!product.isActive());
        }

        if (json != null) {
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();

            serialNumber.setText(jsonObject.get("serialNumber").getAsString());
            inventoryId.setText(jsonObject.get("inventaryID").getAsString());
            latitude.setText(jsonObject.get("latitude").getAsString());
            longitude.setText(jsonObject.get("longitude").getAsString());
            weight.setText(jsonObject.get("weight").getAsString());
            technicalSpecifications.setText(jsonObject.get("technicalSpecifications").getAsString());
            tariffFraction.setText(jsonObject.get("tariffFraction").getAsString());
            description.setText(jsonObject.get("description").getAsString());
            active.setChecked(jsonObject.get("active").getAsBoolean());
            inactive.setChecked(!jsonObject.get("active").getAsBoolean());
        }

        save.setOnClickListener(v -> {
            error.setVisibility(View.GONE);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(save.getWindowToken(), 0);

            if (!ProjectUtils.isOnline(getContext())) {
                ProjectUtils.offlineDialog(getContext());
            } else if (inventoryId.getText().toString().equalsIgnoreCase("")
                    || (!active.isChecked() && !inactive.isChecked())) {
                error.setVisibility(View.VISIBLE);
                error.setText(R.string.required_value_create_product);
                save.setVisibility(View.VISIBLE);
                progressBarLoading.setVisibility(View.GONE);
                inventoryId.setError(getString(R.string.required_value));
                activeRG.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.required));
                handler.postDelayed(() -> activeRG.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.transparent)), 1000);
            } else {
                createProduct();
                save.setVisibility(View.GONE);
                progressBarLoading.setVisibility(View.VISIBLE);
            }
        });


        super.onResume();
    }

    private String getBody() {
        String serialNumberStr, inventoryIdStr, latitudeStr, longitudeStr, weightStr, technicalSpecificationsStr, tariffFractionStr, descriptionStr;
        boolean activeB;

        serialNumberStr = serialNumber.getText().toString();
        inventoryIdStr = inventoryId.getText().toString();
        latitudeStr = latitude.getText().toString();
        longitudeStr = longitude.getText().toString();
        weightStr = weight.getText().toString();
        technicalSpecificationsStr = technicalSpecifications.getText().toString();
        tariffFractionStr = tariffFraction.getText().toString();
        descriptionStr = description.getText().toString();
        activeB = active.isChecked();

        StringBuilder body = new StringBuilder();
        body.append("{ ")
                .append(" \"inventaryID\": \"").append(inventoryIdStr).append("\"").append(", ")
                .append(" \"active\": ").append(activeB);

        if (id != 0) {
            body.append(", ").append(" \"id\": ").append(id);
        }
        if (!serialNumberStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"serialNumber\": \"").append(serialNumberStr).append("\"");
        }
        if (!latitudeStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"latitude\": \"").append(latitudeStr).append("\"");
        }
        if (!longitudeStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"longitude\": \"").append(longitudeStr).append("\"");
        }
        if (!weightStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"weight\": ").append(weightStr);
        }
        if (!technicalSpecificationsStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"technicalSpecifications\": \"").append(technicalSpecificationsStr).append("\"");
        }
        if (!tariffFractionStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"tariffFraction\": ").append(tariffFractionStr);
        }
        if (!descriptionStr.equalsIgnoreCase("")) {
            body.append(", ").append(" \"description\": \"").append(descriptionStr).append("\"");
        }
        body.append(" }");

        return body.toString();
    }

    public void createProduct() {

        TokenInterceptor interceptor = new TokenInterceptor(new Authentication(user).getToken());
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        RestApiAdapter restApiAdapter = new RestApiAdapter();
        Gson gson = restApiAdapter.convertGsonDeserializerProduct();

        EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

        Call<ProductResponse> responseCall;
        if (id == 0)
            responseCall = endpointsApi.createProduct(new JsonParser().parse(getBody()));
        else
            responseCall = endpointsApi.updateProduct(new JsonParser().parse(getBody()));

        responseCall.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProductResponse> call, @NotNull Response<ProductResponse> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, response.message());
                    error.setText(R.string.unknown_error);
                    error.setVisibility(View.VISIBLE);
                    save.setVisibility(View.VISIBLE);
                    progressBarLoading.setVisibility(View.GONE);
                    return;
                }
                ProductResponse productResp = response.body();

                if (productResp != null) {
                    productResponse = productResp.getProduct();
                }

                try {
                    finish();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProductResponse> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(getContext(), getResources().getString(R.string.error_loads_products), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void finish() throws SQLException {
        product = productResponse.getProductEntity();
        databaseHelper.getProductDao();
        if (databaseHelper.readProduct(product.getId()) == null)
            databaseHelper.createProduct(product);
        else
            databaseHelper.updateProduct(product);
        databaseHelper.getProductDao().refresh(product);

        loadFragment(product.getId());

        Log.i(TAG, "LOAD " + product);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void loadFragment(final int idProduct) {
        Runnable mPendingRunnable = () -> {

            Fragment fragment = new DetailsProductFragment(idProduct);

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_product)).addToBackStack(getString(R.string.tag_product));//sync to fix
            fragmentTransaction.commitAllowingStateLoss();
        };

        // If mPendingRunnable is not null, then add to the message queue
        handler.post(mPendingRunnable);
    }

}