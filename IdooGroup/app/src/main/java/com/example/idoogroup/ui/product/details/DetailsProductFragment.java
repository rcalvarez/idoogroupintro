package com.example.idoogroup.ui.product.details;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.Loader;
import android.content.SyncStatusObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.idoogroup.MainActivity;
import com.example.idoogroup.R;
import com.example.idoogroup.api.Authentication;
import com.example.idoogroup.api.EndpointsApi;
import com.example.idoogroup.api.RestApiAdapter;
import com.example.idoogroup.api.TokenInterceptor;
import com.example.idoogroup.api.response.ProductResponse;
import com.example.idoogroup.model.Product;
import com.example.idoogroup.model.SQLiteHelper;
import com.example.idoogroup.model.User;
import com.example.idoogroup.ui.product.createEdit.CreateEditProductFragment;
import com.example.idoogroup.ui.product.list.ListProductFragment;
import com.example.idoogroup.ui.product.list.adapter.ProductsListAdapter;
import com.example.idoogroup.utils.NumberUtils;
import com.example.idoogroup.utils.ProjectUtils;
import com.example.idoogroup.utils.ProxyImage;
import com.example.idoogroup.utils.SaveSharedPreference;
import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsProductFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private final String TAG = "DetailsProductFragment";

    private static final Handler handler = new Handler();
    private final int id;
    private Product product;
    private ProductResponse productResponse;
    private SQLiteHelper databaseHelper;

    private User user;

    private TextView serialNumber, active, inventoryId, latitude, longitude, weight, technicalSpecifications, tariffFraction, description, numberShare, proxyText;
    private LinearLayout share;
    private ImageView iconActive;
    private RecyclerView suggestions;

    private boolean refresh = false;
    private ProgressBar sync;
    /**
     * Create a new anonymous SyncStatusObserver. It's attached to the app's ContentResolver in
     * onResume(), and removed in onPause(). If status changes, it sets the state of the Refresh
     * button. If a sync is active or pending, the Refresh button is replaced by an indeterminate
     * ProgressBar; otherwise, the button itself is displayed.
     */
    private final SyncStatusObserver mSyncStatusObserver = new SyncStatusObserver() {
        /** Callback invoked with the sync adapter status changes. */
        @Override
        public void onStatusChanged(int which) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {

                    /**
                     * The SyncAdapter runs on a background thread. To update the UI, onStatusChanged()
                     * runs on the UI thread.
                     */
                    @Override
                    public void run() {
                        // Create a handle to the account that was created by
                        // SyncService.CreateSyncAccount(). This will be used to query the system to
                        // see how the sync status has changed.
                        Account account = new Account("Account", ProjectUtils.PROJECT_ACCOUNT);
                        if (account == null) {
                            // GetAccount() returned an invalid value. This shouldn't happen, but
                            // we'll set the status to "not refreshing".
                            setRefreshActionButtonState(false);
                            return;
                        }

                        // Test the ContentResolver to see if the sync adapter is active or pending.
                        // Set the state of the refresh button accordingly.
                        boolean syncActive = ContentResolver.isSyncActive(
                                account, ProjectUtils.PROJECT_PROVIDER);
                        boolean syncPending = ContentResolver.isSyncPending(
                                account, ProjectUtils.PROJECT_PROVIDER);
                        setRefreshActionButtonState(syncActive || syncPending);
                    }
                });
            }
        }
    };
    /**
     * Cursor adapter for controlling ListView results.
     */
    private SimpleCursorAdapter mAdapter;
    /**
     * Handle to a SyncObserver. The ProgressBar element is visible until the SyncObserver reports
     * that the sync is complete.
     * <p>
     * <p>This allows us to delete our SyncObserver once the application is no longer in the
     * foreground.
     */
    private Object mSyncObserverHandle;

    public DetailsProductFragment(int id) {
        this.id = id;
    }

    /**
     * Set the state of the Refresh button. If a sync is active, turn on the ProgressBar widget.
     * Otherwise, turn it off.
     *
     * @param refreshing True if an active sync is occuring, false otherwise
     */
    public void setRefreshActionButtonState(boolean refreshing) {
        if (refreshing) {
            if (sync != null) {
                sync.setVisibility(View.VISIBLE);
            }
        } else {
            if (sync != null && !refresh) {
                sync.setVisibility(View.GONE);
                try {
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                    }
                } catch (IllegalStateException e) {
                    Log.i(TAG, "IllegalStateException");
                }
                refresh = true;
            }
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_product, container, false);
        setHasOptionsMenu(true);

        databaseHelper = OpenHelperManager.getHelper(this.getContext(), SQLiteHelper.class);

        sync = root.findViewById(R.id.synchronize);
        proxyText = root.findViewById(R.id.image_proxy);

        serialNumber = root.findViewById(R.id.serial_number);
        iconActive = root.findViewById(R.id.icon_active);
        active = root.findViewById(R.id.active);
        inventoryId = root.findViewById(R.id.inventory_id);
        latitude = root.findViewById(R.id.latitude);
        longitude = root.findViewById(R.id.longitude);
        weight = root.findViewById(R.id.weight);
//        numberShare = root.findViewById(R.id.number_share);
        share = root.findViewById(R.id.share);
        technicalSpecifications = root.findViewById(R.id.technical_specifications);
        tariffFraction = root.findViewById(R.id.tariff_fraction);
        description = root.findViewById(R.id.description);
        suggestions = root.findViewById(R.id.suggestions);

        return root;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {

        MainActivity.selectNavMenu(MainActivity.INDEX_FILM);

        try {
            databaseHelper.getProductDao();
            databaseHelper.getUserDao();
            product = databaseHelper.readProduct(id);
            user = databaseHelper.readUser(SaveSharedPreference.getLoggedInUser(getActivity().getApplicationContext()));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (!refresh) {
            setRefreshActionButtonState(true);
            loadProduct();
        }

        proxyText.setVisibility(View.VISIBLE);
        String productTitle = product.getSerialNumber();
        if (productTitle == null || productTitle.equalsIgnoreCase(""))
            productTitle = "tmp";
        proxyText.setText(ProxyImage.getInitial(productTitle));
        proxyText.setBackground(getResources().getDrawable(ProxyImage.getColor(productTitle)));

        serialNumber.setSelected(true);
        serialNumber.setText(product.getSerialNumber());
        if (getContext() != null && product.isActive()) {
            iconActive.setImageResource(R.drawable.active);
            active.setText("Active");
            active.setTextColor(ContextCompat.getColor(getContext(), R.color.color9));
        } else {
            iconActive.setImageResource(R.drawable.inactive);
            active.setText("Inactive");
            active.setTextColor(ContextCompat.getColor(getContext(), R.color.color0));
        }

        inventoryId.setText(product.getInventoryId());
        latitude.setText(product.getLatitude());
        longitude.setText(product.getLongitude());
        weight.setText(NumberUtils.doubleToStringFormat(product.getWeight()));
        technicalSpecifications.setText(product.getTechnicalSpecifications());
        tariffFraction.setText(NumberUtils.doubleToStringFormat(product.getTariffFraction()));
        description.setText(product.getDescription());
//        numberShare.setText(product.getNumber_shared() + "");

        List<Product> suggestionsList = new ArrayList<>();
        try {
            databaseHelper.getProductDao();
            suggestionsList.addAll(databaseHelper.readSimilarProducts(product));
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        if (getActivity() != null) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            suggestions.setLayoutManager(mLayoutManager);
        }
        suggestions.setNestedScrollingEnabled(false);
        suggestions.setMinimumHeight(440 *  suggestionsList.size());
        ProductsListAdapter productsListAdapter = new ProductsListAdapter(suggestionsList, getActivity());
        suggestions.setAdapter(productsListAdapter);

        share.setOnClickListener(v -> {
            if (getContext() != null) {
                final Dialog shareOptions = new Dialog(getContext());
                shareOptions.setContentView(R.layout.options_shared);

                LinearLayout facebook = shareOptions.findViewById(R.id.facebook);
                LinearLayout twitter = shareOptions.findViewById(R.id.twitter);
                LinearLayout whatsApp = shareOptions.findViewById(R.id.whatsapp);

                facebook.setOnClickListener(v1 -> {
//                    ProjectUtils.shareInFacebook(getContext(), film, databaseHelper, numberShare);
                    shareOptions.dismiss();
                });

                twitter.setOnClickListener(v12 -> {
//                    ProjectUtils.shareInTwitter(getContext(), film, databaseHelper, numberShare);
                    shareOptions.dismiss();
                });

                whatsApp.setOnClickListener(v13 -> {
//                    ProjectUtils.shareInWhatsApp(getContext(), film, databaseHelper, numberShare);
                    shareOptions.dismiss();
                });
                shareOptions.show();
            }
        });

        super.onResume();
    }

    public void loadProduct() {

        TokenInterceptor interceptor = new TokenInterceptor(new Authentication(user).getToken());
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        RestApiAdapter restApiAdapter = new RestApiAdapter();
        Gson gson = restApiAdapter.convertGsonDeserializerProduct();

        EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

        Call<ProductResponse> responseCall = endpointsApi.getProduct(product.getId());

        responseCall.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProductResponse> call, @NotNull Response<ProductResponse> response) {
                ProductResponse productResp = response.body();

                if (productResp != null) {
                    productResponse = productResp.getProduct();
                }

                try {
                    finish();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProductResponse> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(getContext(), getResources().getString(R.string.error_loads_products), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void finish() throws SQLException {
        product = productResponse.getProductEntity();
        databaseHelper.getProductDao();
        if (databaseHelper.readProduct(product.getId()) == null)
            databaseHelper.createProduct(product);
        else
            databaseHelper.updateProduct(product);

        Log.i(TAG, "LOAD " + product);
        setRefreshActionButtonState(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSyncObserverHandle != null) {
            ContentResolver.removeStatusChangeListener(mSyncObserverHandle);
            mSyncObserverHandle = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_create) {
            Runnable mPendingRunnable = () -> {
                // update the main content by replacing fragments
                Fragment fragment = new CreateEditProductFragment();
                if (getFragmentManager() != null) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_create_edit)).addToBackStack(getString(R.string.tag_create_edit));//sync to fix
                    fragmentTransaction.commitAllowingStateLoss();
                }
            };
            // If mPendingRunnable is not null, then add to the message queue
            handler.post(mPendingRunnable);
            return true;
        } else if (id == R.id.action_edit) {
            editAction();
            return true;
        } else if (id == R.id.action_delete) {
            deleteAction();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void editAction() {
        Runnable mPendingRunnable = () -> {
            // update the main content by replacing fragments
            Fragment fragment = new CreateEditProductFragment(product.getId());
            if (getFragmentManager() != null) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_create_edit)).addToBackStack(getString(R.string.tag_create_edit));//sync to fix
                fragmentTransaction.commitAllowingStateLoss();
            }
        };
        // If mPendingRunnable is not null, then add to the message queue
        handler.post(mPendingRunnable);
    }

    private void deleteAction() {
        final Dialog confirmRemove = new Dialog(getContext());
        confirmRemove.setContentView(R.layout.confirm_remove);

        TextView title = confirmRemove.findViewById(R.id.title);
        TextView content = confirmRemove.findViewById(R.id.content);

        title.setText(R.string.title_confirm_remove_product);
        content.setText(R.string.content_confirm_remove_product);

        Button yes = confirmRemove.findViewById(R.id.yes);
        Button no = confirmRemove.findViewById(R.id.no);

        yes.setOnClickListener(v -> {
            TokenInterceptor interceptor = new TokenInterceptor(new Authentication(user).getToken());
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            RestApiAdapter restApiAdapter = new RestApiAdapter();
            Gson gson = restApiAdapter.convertGsonDeserializerProduct();

            EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

            Call<ProductResponse> responseCall = endpointsApi.deleteProduct(product.getId());

            responseCall.enqueue(new Callback<ProductResponse>() {
                @Override
                public void onResponse(@NotNull Call<ProductResponse> call, @NotNull Response<ProductResponse> response) {
                    ProductResponse productResp = response.body();
                    if (!response.isSuccessful()) {
                        Log.e(TAG, response.message());
                        Toast.makeText(getContext(), "ERROR DELETE PRODUCT", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    try {
                        finishDelete();
                    } catch (SQLException throwable) {
                        throwable.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<ProductResponse> call, @NotNull Throwable t) {
                    Log.e(TAG, t.toString());
                    Toast.makeText(getContext(), getResources().getString(R.string.error_loads_products), Toast.LENGTH_LONG).show();
                }
            });

            confirmRemove.dismiss();
        });

        no.setOnClickListener(v -> confirmRemove.dismiss());
        confirmRemove.show();
    }

    private void finishDelete() throws SQLException {
        databaseHelper.getProductDao();

        if (databaseHelper.deleteProduct(product) > 0) {
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.product_deleted), Toast.LENGTH_LONG).show();
            Runnable mPendingRunnable = () -> {

                Fragment fragment = new ListProductFragment();

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_products)).addToBackStack(getString(R.string.tag_products));//sync to fix
                fragmentTransaction.commitAllowingStateLoss();
            };

            // If mPendingRunnable is not null, then add to the message queue
            handler.post(mPendingRunnable);
        } else
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.unknown_error), Toast.LENGTH_LONG).show();

        Log.i(TAG, "DELETE SUCCESS");
        setRefreshActionButtonState(false);
    }

}