package com.example.idoogroup.ui.product.list;

import android.accounts.Account;
import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.Loader;
import android.content.SyncStatusObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.idoogroup.MainActivity;
import com.example.idoogroup.R;
import com.example.idoogroup.api.Authentication;
import com.example.idoogroup.api.EndpointsApi;
import com.example.idoogroup.api.RestApiAdapter;
import com.example.idoogroup.api.TokenInterceptor;
import com.example.idoogroup.api.response.ProductsResponse;
import com.example.idoogroup.model.Product;
import com.example.idoogroup.model.SQLiteHelper;
import com.example.idoogroup.model.User;
import com.example.idoogroup.ui.product.list.adapter.ProductsListAdapter;
import com.example.idoogroup.utils.ProjectUtils;
import com.example.idoogroup.utils.SaveSharedPreference;
import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListProductFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "ListProductFragment";

    private static final Handler handler = new Handler();
    private final List<Product> productList = new ArrayList<>();
    private ProductsListAdapter pAdapter;
    private TextView nonProduct;
    private SQLiteHelper databaseHelper;
    private List<Product> allProductList = new ArrayList<>();
    private RecyclerView recyclerView;
    private int initPosition = 0;
    private int amount = 15;
    private int totalProduct;
    private boolean hasMore = true;

    private ArrayList<Product> products;
    private User user;

    private boolean refresh = false;
    private ProgressBar sync;
    /**
     * Create a new anonymous SyncStatusObserver. It's attached to the app's ContentResolver in
     * onResume(), and removed in onPause(). If status changes, it sets the state of the Refresh
     * button. If a sync is active or pending, the Refresh button is replaced by an indeterminate
     * ProgressBar; otherwise, the button itself is displayed.
     */
    private final SyncStatusObserver mSyncStatusObserver = new SyncStatusObserver() {
        /** Callback invoked with the sync adapter status changes. */
        @Override
        public void onStatusChanged(int which) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {

                    /**
                     * The SyncAdapter runs on a background thread. To update the UI, onStatusChanged()
                     * runs on the UI thread.
                     */
                    @Override
                    public void run() {
                        // Create a handle to the account that was created by
                        // SyncService.CreateSyncAccount(). This will be used to query the system to
                        // see how the sync status has changed.
                        Account account = new Account("Account", ProjectUtils.PROJECT_ACCOUNT);
                        if (account == null) {
                            // GetAccount() returned an invalid value. This shouldn't happen, but
                            // we'll set the status to "not refreshing".
                            setRefreshActionButtonState(false);
                            return;
                        }

                        // Test the ContentResolver to see if the sync adapter is active or pending.
                        // Set the state of the refresh button accordingly.
                        boolean syncActive = ContentResolver.isSyncActive(
                                account, ProjectUtils.PROJECT_PROVIDER);
                        boolean syncPending = ContentResolver.isSyncPending(
                                account, ProjectUtils.PROJECT_PROVIDER);
                        setRefreshActionButtonState(syncActive || syncPending);
                    }
                });
            }
        }
    };
    /**
     * Cursor adapter for controlling ListView results.
     */
    private SimpleCursorAdapter mAdapter;
    /**
     * Handle to a SyncObserver. The ProgressBar element is visible until the SyncObserver reports
     * that the sync is complete.
     * <p>
     * <p>This allows us to delete our SyncObserver once the application is no longer in the
     * foreground.
     */
    private Object mSyncObserverHandle;

    /**
     * Set the state of the Refresh button. If a sync is active, turn on the ProgressBar widget.
     * Otherwise, turn it off.
     *
     * @param refreshing True if an active sync is occuring, false otherwise
     */
    public void setRefreshActionButtonState(boolean refreshing) {
        if (refreshing) {
            if (sync != null) {
                sync.setVisibility(View.VISIBLE);
            }
        } else {
            if (sync != null && !refresh) {
                sync.setVisibility(View.GONE);
                try {
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                    }
                } catch (IllegalStateException e) {
                    Log.i(TAG, "IllegalStateException");
                }
                refresh = true;
            }
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_product_list, container, false);

        databaseHelper = OpenHelperManager.getHelper(this.getContext(), SQLiteHelper.class);

        recyclerView = root.findViewById(R.id.recycler_view);
        nonProduct = root.findViewById(R.id.non_product);
        sync = root.findViewById(R.id.synchronize);

        return root;
    }

    @Override
    public void onResume() {

        MainActivity.selectNavMenu(MainActivity.INDEX_FILMS);

        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this.getContext(), SQLiteHelper.class);
        }

        allProductList.clear();
        productList.clear();

        try {
            if (getContext() != null)
                user = databaseHelper.readUser(SaveSharedPreference.getLoggedInUser(getContext().getApplicationContext()));
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        if (!refresh) {
            setRefreshActionButtonState(true);
            loadProducts();
        } else {
            initPosition = 0;
            amount = 15;
            hasMore = true;
        }

        try {
            databaseHelper.getProductDao();
            allProductList = databaseHelper.readAllProduct();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        totalProduct = allProductList.size();
        if (totalProduct < amount) {
            amount = totalProduct;
            hasMore = false;
        }
        productList.addAll(allProductList.subList(initPosition, initPosition + amount));
        if (amount != 15) {
            amount = 0;
        }
        initPosition += amount;
        if (hasMore) {
            if (totalProduct < initPosition + amount) {
                amount = totalProduct - initPosition;
                hasMore = false;
            }
        }

        if (productList.size() == 0) {
            nonProduct.setVisibility(View.VISIBLE);
        }

        if (getActivity() != null) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
        }
        recyclerView.setNestedScrollingEnabled(false);
        pAdapter = new ProductsListAdapter(productList, recyclerView, getActivity());
        recyclerView.setAdapter(pAdapter);
        pAdapter.setOnLoadMoreListener(() -> {
            if (hasMore || amount != 0) {
                productList.add(null);
                pAdapter.notifyItemInserted(productList.size() - 1);
                handler.postDelayed(() -> {
                    boolean intoElse = false;
                    productList.remove(productList.size() - 1);
                    pAdapter.notifyItemRemoved(productList.size());
                    productList.addAll(allProductList.subList(initPosition, initPosition + amount));
                    if (hasMore) {
                        initPosition += amount;
                        if (totalProduct < initPosition + amount) {
                            amount = totalProduct - initPosition;
                            intoElse = true;
                            hasMore = false;
                        }
                    }
                    if (!intoElse && amount != 15) {
                        amount = 0;
                    }
                    pAdapter.notifyItemInserted(productList.size());
                    pAdapter.setLoaded();
                }, 2000);
            }
        });

        super.onResume();
    }

    public void loadProducts() {
        TokenInterceptor interceptor = new TokenInterceptor(new Authentication(user).getToken());
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        RestApiAdapter restApiAdapter = new RestApiAdapter();
        Gson gson = restApiAdapter.convertGsonDeserializerProducts();

        EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

        Call<ProductsResponse> responseCall = endpointsApi.getProducts();

        responseCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProductsResponse> call, @NotNull Response<ProductsResponse> response) {
                ProductsResponse productsResponse = response.body();

                products = new ArrayList<>();

                if (productsResponse != null) {
                    products = productsResponse.getProducts();
                }

                try {
                    Log.i(TAG, "READ " + products.size() + " products");
                    finish();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProductsResponse> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(getContext(), getResources().getString(R.string.error_loads_products), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void finish() throws SQLException {
        Log.i(TAG, products.size() + " to create");

        databaseHelper.getProductDao();
        for (Product product :
                products) {
            if (databaseHelper.readProduct(product.getId()) == null)
                databaseHelper.createProduct(product);
            else
                databaseHelper.updateProduct(product);
        }
        Log.i(TAG, "CREATED " + products.size() + " products");
        setRefreshActionButtonState(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSyncObserverHandle != null) {
            ContentResolver.removeStatusChangeListener(mSyncObserverHandle);
            mSyncObserverHandle = null;
        }
    }
}