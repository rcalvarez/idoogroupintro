package com.example.idoogroup.ui.product.list.adapter;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.idoogroup.R;
import com.example.idoogroup.model.Product;
import com.example.idoogroup.ui.product.details.DetailsProductFragment;
import com.example.idoogroup.ui.product.list.viewHolders.ListProductRecyclerViewHolder;
import com.example.idoogroup.ui.product.list.viewHolders.ProgressViewHolder;
import com.example.idoogroup.utils.NumberUtils;
import com.example.idoogroup.utils.OnLoadMoreListener;
import com.example.idoogroup.utils.ProxyImage;

import org.jetbrains.annotations.NotNull;

import java.util.List;


@SuppressLint("StaticFieldLeak")
public class ProductsListAdapter extends RecyclerView.Adapter<ListProductRecyclerViewHolder> {

    private final List<Product> productList;
    private final Handler handler = new Handler();
    //para infinite scrolling
    private final FragmentActivity fragmentActivity; // to handle fragments on click
    private final int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public ProductsListAdapter(List<Product> productList, FragmentActivity fragmentActivity) {
        this.productList = productList;
        this.fragmentActivity = fragmentActivity;
    }

    public ProductsListAdapter(List<Product> productList, RecyclerView recyclerView, FragmentActivity fragmentActivity) {
        this.productList = productList;
        this.fragmentActivity = fragmentActivity;
        //infinite scrolling
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public ListProductRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListProductRecyclerViewHolder viewHolder;
        if (viewType == 1) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
            viewHolder = new ListProductRecyclerViewHolder(layoutView);
        } else {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            viewHolder = new ProgressViewHolder(layoutView);
        }

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ListProductRecyclerViewHolder holder, final int position) {
        Product product = productList.get(position);

        //infinite scrolling
        if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        } else {
            holder.inventoryId.setText(product.getInventoryId());
            holder.serialNumber.setText(product.getSerialNumber());
            if (product.isActive()) {
                holder.active.setText("Active");
                holder.active.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.color9));
            } else {
                holder.active.setText("Inactive");
                holder.active.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.color0));
            }
            holder.weight.setText(NumberUtils.doubleToStringFormat(product.getWeight()));
            holder.tariffFraction.setText(NumberUtils.doubleToStringFormat(product.getTariffFraction()));

            if (product.getImageItem() != null) {
                holder.logo.setVisibility(View.GONE);
                holder.proxyText.setVisibility(View.VISIBLE);
                String title = product.getSerialNumber();
                if (title == null || title.equalsIgnoreCase(""))
                    title = "tmp";
                holder.proxyText.setText(ProxyImage.getInitial(title));
                holder.proxyText.setBackground(fragmentActivity.getResources().getDrawable(ProxyImage.getDrawer(title)));
            } else {
                holder.proxyText.setVisibility(View.GONE);
                holder.logo.setVisibility(View.VISIBLE);
//                ImagesUtils.loadImageFromUrl(itemView.getContext(), place.getLogo(), holder.placeLogo, ImagesUtils.CIRCLE_IMAGE);
            }

            holder.itemView.setOnClickListener(v -> loadFragment(productList.get(position).getId()));
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    //methods for infinite scrolling
    @Override
    public int getItemViewType(int position) {
        return productList.get(position) != null ? 1 : 0;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    private void loadFragment(final int idProduct) {
        Runnable mPendingRunnable = () -> {

            Fragment fragment = new DetailsProductFragment(idProduct);

            FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment, fragmentActivity.getString(R.string.tag_product)).addToBackStack(fragmentActivity.getString(R.string.tag_product));//sync to fix
            fragmentTransaction.commitAllowingStateLoss();
        };

        // If mPendingRunnable is not null, then add to the message queue
        handler.post(mPendingRunnable);
    }


}