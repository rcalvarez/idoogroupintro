package com.example.idoogroup.ui.product.list.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.idoogroup.R;


public class ListProductRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView inventoryId, serialNumber, active, weight, tariffFraction, proxyText;
    public ImageView logo;

    public ListProductRecyclerViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        inventoryId = itemView.findViewById(R.id.inventory_id);
        serialNumber = itemView.findViewById(R.id.serial_number);
        active = itemView.findViewById(R.id.active);
        weight = itemView.findViewById(R.id.weight);
        tariffFraction = itemView.findViewById(R.id.tariff_fraction);
        proxyText = itemView.findViewById(R.id.image_proxy);
        logo = itemView.findViewById(R.id.film_logo);
    }

    @Override
    public void onClick(View view) {
    }
}