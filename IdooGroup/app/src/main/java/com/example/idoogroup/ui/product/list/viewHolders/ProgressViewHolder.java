package com.example.idoogroup.ui.product.list.viewHolders;


import android.view.View;
import android.widget.ProgressBar;

import com.example.idoogroup.R;

public class ProgressViewHolder extends ListProductRecyclerViewHolder {
    public ProgressBar progressBar;

    public ProgressViewHolder(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
    }
}