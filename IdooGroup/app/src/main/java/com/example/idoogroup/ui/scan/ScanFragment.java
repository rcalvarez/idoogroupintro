package com.example.idoogroup.ui.scan;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.idoogroup.MainActivity;
import com.example.idoogroup.R;
import com.example.idoogroup.api.Authentication;
import com.example.idoogroup.api.EndpointsApi;
import com.example.idoogroup.api.RestApiAdapter;
import com.example.idoogroup.api.TokenInterceptor;
import com.example.idoogroup.api.response.ProductResponse;
import com.example.idoogroup.model.Product;
import com.example.idoogroup.model.SQLiteHelper;
import com.example.idoogroup.model.User;
import com.example.idoogroup.ui.product.createEdit.CreateEditProductFragment;
import com.example.idoogroup.ui.product.list.ListProductFragment;
import com.example.idoogroup.utils.SaveSharedPreference;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.zxing.Result;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScanFragment extends Fragment implements ZXingScannerView.ResultHandler {

    private static final Handler handler = new Handler();

    private View root;
    private final String TAG = "ScanFragment";
    private SQLiteHelper databaseHelper;

    private ZXingScannerView scan;

    private Product product;
    private User user;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_scan, container, false);

        databaseHelper = OpenHelperManager.getHelper(this.getContext(), SQLiteHelper.class);

        scan = root.findViewById(R.id.scan);

        // the code below appears in onCreate() method
        if (getContext() != null && getActivity() != null && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 123);
        } else {
            startScanning();
        }

        return root;
    }

    private void startScanning() {
        scan.setResultHandler(this);
        scan.setAutoFocus(true);
        scan.startCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Camera permission granted", Toast.LENGTH_LONG).show();
                startScanning();
            } else {
                Toast.makeText(getContext(), "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume() {

        MainActivity.selectNavMenu(MainActivity.INDEX_SCAN);

        try {
            databaseHelper.getUserDao();
            user = databaseHelper.readUser(SaveSharedPreference.getLoggedInUser(getActivity().getApplicationContext()));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        super.onResume();
        scan.resumeCameraPreview(this);
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            showDialog(rawResult.getText());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            scan.resumeCameraPreview(this);
        }

    }

    @Override
    public void onPause() {
        scan.stopCamera();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        scan.stopCamera();
        super.onDestroy();
    }

    private void showDialog(String text) {
        if (getContext() != null) {
            final Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.dialog_show_qr);

            TextView textQr = dialog.findViewById(R.id.text_qr);
            final Button rescan = dialog.findViewById(R.id.rescan);
            Button action = dialog.findViewById(R.id.action);

            // if button is clicked, close the custom dialog
            rescan.setOnClickListener(v -> {
                dialog.dismiss();
                scan.resumeCameraPreview(this);
            });

            String actionStr = new JsonParser().parse(text).getAsJsonObject().get("action").getAsString();
            action.setText(actionStr);
            textQr.setText(text);

            action.setOnClickListener(v -> {
                Runnable mPendingRunnable;// update the main content by replacing fragments
                if (actionStr.equalsIgnoreCase("create")) {
                    mPendingRunnable = () -> {
                        // update the main content by replacing fragments
                        Fragment fragment = new CreateEditProductFragment(text);
                        if (getFragmentManager() != null) {
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                    android.R.anim.fade_out);
                            fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_create_edit)).addToBackStack(getString(R.string.tag_create_edit));//sync to fix
                            fragmentTransaction.commitAllowingStateLoss();
                        }
                    };
                } else if (actionStr.equalsIgnoreCase("edit")) {
                    mPendingRunnable = () -> {
                        // update the main content by replacing fragments
                        Fragment fragment = new CreateEditProductFragment(new JsonParser().parse(text).getAsJsonObject().get("id").getAsInt(), text);
                        if (getFragmentManager() != null) {
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                    android.R.anim.fade_out);
                            fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_create_edit)).addToBackStack(getString(R.string.tag_create_edit));//sync to fix
                            fragmentTransaction.commitAllowingStateLoss();
                        }
                    };
                } else {
                    mPendingRunnable = () -> deleteAction(text);
                }
                handler.post(mPendingRunnable);

                dialog.dismiss();
            });

            dialog.show();
        }
    }

    private void deleteAction(String text) {
        final Dialog confirmRemove = new Dialog(getContext());
        confirmRemove.setContentView(R.layout.confirm_remove);

        TextView title = confirmRemove.findViewById(R.id.title);
        TextView content = confirmRemove.findViewById(R.id.content);

        title.setText(R.string.title_confirm_remove_product);
        content.setText(R.string.content_confirm_remove_product);

        Button yes = confirmRemove.findViewById(R.id.yes);
        Button no = confirmRemove.findViewById(R.id.no);

        try {
            databaseHelper.getProductDao();
            product = databaseHelper.readProduct(new JsonParser().parse(text).getAsJsonObject().get("id").getAsInt());
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        yes.setOnClickListener(v -> {
            TokenInterceptor interceptor = new TokenInterceptor(new Authentication(user).getToken());
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            RestApiAdapter restApiAdapter = new RestApiAdapter();
            Gson gson = restApiAdapter.convertGsonDeserializerProduct();

            EndpointsApi endpointsApi = restApiAdapter.setUpConnectionRestApi(gson, client);

            Call<ProductResponse> responseCall = endpointsApi.deleteProduct(new JsonParser().parse(text).getAsJsonObject().get("id").getAsInt());

            responseCall.enqueue(new Callback<ProductResponse>() {
                @Override
                public void onResponse(@NotNull Call<ProductResponse> call, @NotNull Response<ProductResponse> response) {
                    ProductResponse productResp = response.body();
                    if (!response.isSuccessful()) {
                        Log.e(TAG, response.message());
                        Toast.makeText(getContext(), "ERROR DELETE PRODUCT", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    try {
                        finishDelete();
                    } catch (SQLException throwable) {
                        throwable.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<ProductResponse> call, @NotNull Throwable t) {
                    Log.e(TAG, t.toString());
                    Toast.makeText(getContext(), getResources().getString(R.string.error_loads_products), Toast.LENGTH_LONG).show();
                }
            });

            confirmRemove.dismiss();
        });

        no.setOnClickListener(v -> confirmRemove.dismiss());
        confirmRemove.show();
    }

    private void finishDelete() throws SQLException {
        databaseHelper.getProductDao();

        if (databaseHelper.deleteProduct(product) > 0) {
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.product_deleted), Toast.LENGTH_LONG).show();
            Runnable mPendingRunnable = () -> {

                Fragment fragment = new ListProductFragment();

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, getString(R.string.tag_products)).addToBackStack(getString(R.string.tag_products));//sync to fix
                fragmentTransaction.commitAllowingStateLoss();
            };

            // If mPendingRunnable is not null, then add to the message queue
            handler.post(mPendingRunnable);
        } else
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.unknown_error), Toast.LENGTH_LONG).show();

        Log.i(TAG, "DELETE SUCCESS");
    }


}