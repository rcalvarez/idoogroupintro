package com.example.idoogroup.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * Created by Leonar2 on 8/5/2018.
 */

public class DateUtils {
    public static Date getCurrentDate() {
        return new Date();
    }

    public static String toLongResolution(Date date) {
        Locale locale = new Locale("es");
        SimpleDateFormat df = new SimpleDateFormat("EE dd 'de' MMMM, yyyy K:m a", locale);
        return df.format(date);
    }

    public static String toDayResolution(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return df.format(date);
    }

    public static String toHourResolution(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH", Locale.getDefault());
        return df.format(date);
    }

    public static String toMinuteResolution(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
        return df.format(date);
    }

    public static String toSecondResolution(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        return df.format(date);
    }

    public static Date ApiDateToDate(String date) {
        if (date == null || date.equals("")) {
            return null;
        }
        TimeZone tz = TimeZone.getDefault();
        // parsing "2018-09-29T17:57:17.161255"
        // milliseconds are lost while parsing
        StringTokenizer token = new StringTokenizer(date);
        int year = Integer.parseInt(token.nextToken("-"));
        int month = Integer.parseInt(token.nextToken("-"));
        int day = Integer.parseInt(token.nextToken("-T"));
        int hour = Integer.parseInt(token.nextToken("T:"));
        int min = Integer.parseInt(token.nextToken(":"));
        int sec = Integer.parseInt(token.nextToken(":."));
//        int millis = Integer.parseInt(token.nextToken());
//        System.out.println(Integer.parseInt(year) + " " + Integer.parseInt(month) + " " + Integer.parseInt(day) + " " + Integer.parseInt(hour) + " " + Integer.parseInt(min) + " " + Integer.parseInt(sec) + " " + millis);
//        long milliseconds = millis + 1000*(sec + 60*(min + 60*(hour +24*(day + )) ));
        return new Date((year) - 1900, (month) - 1, (day), (hour), (min), (sec));
    }

    public static Date ApiDateVariableToDate(String date) {
        if (date == null || date.equals("")) {
            return null;
        }
        TimeZone tz = TimeZone.getDefault();
        // parsing "2020-01-20T11:53:59-05:00"
        // milliseconds are lost while parsing
        StringTokenizer token = new StringTokenizer(date);
        int year = Integer.parseInt(token.nextToken("-"));
        int month = Integer.parseInt(token.nextToken("-"));
        int day = Integer.parseInt(token.nextToken("-T"));
        int hour = Integer.parseInt(token.nextToken("T:"));
        int min = Integer.parseInt(token.nextToken(":"));
        String tmp = token.nextToken(":-");
        int sec;
        if (tmp.contains(".")) {
            sec = Integer.parseInt(new StringTokenizer(tmp).nextToken("."));
        } else {
            sec = Integer.parseInt(tmp);
        }
//        int millis = Integer.parseInt(token.nextToken());
//        System.out.println(Integer.parseInt(year) + " " + Integer.parseInt(month) + " " + Integer.parseInt(day) + " " + Integer.parseInt(hour) + " " + Integer.parseInt(min) + " " + Integer.parseInt(sec) + " " + millis);
//        long milliseconds = millis + 1000*(sec + 60*(min + 60*(hour +24*(day + )) ));
        return new Date((year) - 1900, (month) - 1, (day), (hour), (min), (sec));
    }

    public static Date ApiSmallDateToDate(String date) {
        if (date == null || date.equals("")) {
            return null;
        }
        // parsing "2018-09-29T17:57:17.161255"
        // milliseconds are lost while parsing
        StringTokenizer token = new StringTokenizer(date);
        int year = Integer.parseInt(token.nextToken("-"));
        int month = Integer.parseInt(token.nextToken("-"));
        int day = Integer.parseInt(token.nextToken());
//        System.out.println(Integer.parseInt(year) + " " + Integer.parseInt(month) + " " + Integer.parseInt(day) + " " + Integer.parseInt(hour) + " " + Integer.parseInt(min) + " " + Integer.parseInt(sec) + " " + millis);
//        long milliseconds = millis + 1000*(sec + 60*(min + 60*(hour +24*(day + )) ));
        return new Date((year) - 1900, (month) - 1, (day));
    }

    public static String dateToApiDate(Date date) {
        if (date == null) {
            return "";
        }
        return (date.getYear() + 1900) + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "T" + date.getHours() + ":" +
                date.getMinutes() + ":" + date.getSeconds();
    }

    public static String dateToApiSmallDate(Date date) {
        if (date == null) {
            return "";
        }
        return (date.getYear() + 1900) + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }

    public static Date stringToDate(String date) {
        if (date == null || date.equals("")) {
            return null;
        }
        // parsing "09-29-2018"
        StringTokenizer token = new StringTokenizer(date);
        int year = Integer.parseInt(token.nextToken("-"));
        int month = Integer.parseInt(token.nextToken("-"));
        int day = Integer.parseInt(token.nextToken());
//        System.out.println(Integer.parseInt(year) + " " + Integer.parseInt(month) + " " + Integer.parseInt(day) + " " + Integer.parseInt(hour) + " " + Integer.parseInt(min) + " " + Integer.parseInt(sec) + " " + millis);
//        long milliseconds = millis + 1000*(sec + 60*(min + 60*(hour +24*(day + )) ));
        return new Date((year) - 1900, (month) - 1, (day));
    }

    public static String dateToString(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return df.format(date);
    }
}
