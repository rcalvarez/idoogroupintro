package com.example.idoogroup.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


public class NumberUtils {

    public static String intToString(int number) {
        return number + "";
    }

    public static String doubleToString(double number) {
        return number + "";
    }

    public static int stringToInt(String number) {
        return Integer.parseInt(number);
    }

    public static double stringToDouble(String number) {
        return Double.parseDouble(number);
    }

    public static String intToStringFormat(int number) {
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);

        return df.format(number);
    }

    public static String doubleToStringFormat(double number) {
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);

        return df.format(number);
    }
}