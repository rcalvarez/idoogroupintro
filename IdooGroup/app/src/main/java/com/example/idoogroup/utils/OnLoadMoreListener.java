package com.example.idoogroup.utils;

public interface OnLoadMoreListener {
    void onLoadMore();
}
