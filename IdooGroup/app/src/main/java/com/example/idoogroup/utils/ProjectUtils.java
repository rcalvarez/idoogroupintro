package com.example.idoogroup.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.widget.Button;
import android.widget.ImageView;

import com.example.idoogroup.R;

public class ProjectUtils {
    public static final String PROJECT_ACCOUNT = "com.example.idoogroup.account";
    public static final String PROJECT_PROVIDER = "com.example.idoogroup.provider";
    public static final String FILM = "film";
    public static final String URL = "url";
    private static final String FACEBOOK = "facebook";
    private static final String TWITTER = "twitter";
    private static final String WHATS_APP = "whats_app";


    /**
     * Use this method to check if there is a connection
     *
     * @param context Context instance
     * @return true if there is connection or false otherwise
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager conn_manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (conn_manager != null) {
            networkInfo = conn_manager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    /**
     * Use this method to show an offline dialog
     *
     * @param context Context instance.
     */
    public static void offlineDialog(final Context context) {
        final Dialog offlineDialog = new Dialog(context);
        offlineDialog.setContentView(R.layout.offline_dialog);
        Button goConfig = offlineDialog.findViewById(R.id.config);
        ImageView close = offlineDialog.findViewById(R.id.close);
        goConfig.setOnClickListener(v -> {
            context.startActivity(new Intent(Settings.ACTION_SETTINGS));
            offlineDialog.dismiss();
        });
        close.setOnClickListener(v -> offlineDialog.dismiss());
        offlineDialog.show();
    }

//    /**
//     * Use this method to share content on social networks (Facebook, Twitter or WhatsApp)
//     *
//     * @param target         string representing social network. Use <br>
//     *                       UtilsGoospe.FACEBOOK -> "facebook" <br>
//     *                       UtilsGoospe.TWITTER -> "twitter" <br>
//     *                       UtilsGoospe.WHATS_APP -> "whats_app"
//     * @param context        Context instance
//     * @param film           Film to be shared
//     * @param databaseHelper SQLiteHelper to data base connection
//     * @param numberShare    TextView instance where it will be show number of shared
//     * @throws Exception if you can't launch the share app
//     */
//    private static void shareUrl(String target, Context context, Film film, SQLiteHelper databaseHelper, TextView numberShare) throws Exception {
//        boolean shared = true;
//
//        if (isOnline(context)) {
//            switch (target) {
//                case FACEBOOK:
//                    try {
//                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                        sharingIntent.setType("text/plain");
//                        sharingIntent.putExtra(Intent.EXTRA_TEXT, film.getUrl());
//                        sharingIntent.setPackage("com.facebook.katana");
//                        context.startActivity(sharingIntent);
//                    } catch (Exception e) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setData(Uri.parse(film.getUrl()));
//                        context.startActivity(intent);
//                    }
//                    break;
//                case TWITTER:
//                    try {
//                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                        sharingIntent.setType("text/plain");
//                        sharingIntent.putExtra(Intent.EXTRA_TEXT, film.getUrl());
//                        sharingIntent.setPackage("advanced.twitter.android");
//                        context.startActivity(sharingIntent);
//                    } catch (Exception e) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setData(Uri.parse(film.getUrl()));
//                        context.startActivity(intent);
//                    }
//                    break;
//                case WHATS_APP:
//                    try {
//                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                        sharingIntent.setType("text/plain");
//                        sharingIntent.putExtra(Intent.EXTRA_TEXT, film.getUrl());
//                        sharingIntent.setPackage("com.whatsapp");
//                        context.startActivity(sharingIntent);
//                    } catch (Exception e) {
//                        shared = false;
//                        Toast.makeText(context, context.getString(R.string.non_whatsapp_installer), Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//                default:
//                    throw new Exception("Target not supported");
//            }
//
//            if (shared) {
//                try {
//                    databaseHelper.getFilmDao().refresh(film);
//                    film.setNumber_shared(film.getNumber_shared() + 1);
//                    if (databaseHelper.updateFilm(film) == 0) {
//                        Toast.makeText(context, context.getString(R.string.sync_error), Toast.LENGTH_SHORT).show();
//                    } else {
//                        switch (target) {
//                            case FACEBOOK:
//                                Toast.makeText(context, context.getString(R.string.sharing_facebook), Toast.LENGTH_SHORT).show();
//                                break;
//                            case TWITTER:
//                                Toast.makeText(context, context.getString(R.string.sharing_twitter), Toast.LENGTH_SHORT).show();
//                                break;
//                            case WHATS_APP:
//                                Toast.makeText(context, context.getString(R.string.sharing_whatsapp), Toast.LENGTH_SHORT).show();
//                                break;
//                            default:
//                                throw new Exception("Target not supported");
//                        }
//                        databaseHelper.getFilmDao().refresh(film);
//                        numberShare.setText(film.getNumber_shared());
//                    }
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//        } else {
//            offlineDialog(context);
//        }
//    }

//    /**
//     * Use this method to share content on Facebook
//     *
//     * @param context        Context instance
//     * @param film           Film to be shared
//     * @param databaseHelper SQLiteHelper to data base connection
//     * @param numberShare    TextView instance where it will be show number of shared
//     */
//    public static void shareInFacebook(Context context, Film film, SQLiteHelper databaseHelper, TextView numberShare) {
//        try {
//            shareUrl(FACEBOOK, context, film, databaseHelper, numberShare);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    /**
//     * Use this method to share content on Twitter
//     *
//     * @param context        Context instance
//     * @param film           Film to be shared
//     * @param databaseHelper SQLiteHelper to data base connection
//     * @param numberShare    TextView instance where it will be show number of shared
//     */
//    public static void shareInTwitter(Context context, Film film, SQLiteHelper databaseHelper, TextView numberShare) {
//        try {
//            shareUrl(TWITTER, context, film, databaseHelper, numberShare);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    /**
//     * Use this method to share content on WhatsApp
//     *
//     * @param context        Context instance
//     * @param film           Film to be shared
//     * @param databaseHelper SQLiteHelper to data base connection
//     * @param numberShare    TextView instance where it will be show number of shared
//     */
//    public static void shareInWhatsApp(Context context, Film film, SQLiteHelper databaseHelper, TextView numberShare) {
//        try {
//            shareUrl(WHATS_APP, context, film, databaseHelper, numberShare);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
