package com.example.idoogroup.utils;

import com.example.idoogroup.R;


public class ProxyImage {

    public static String getInitial(String title) {
        return title.toUpperCase().substring(0, 1);
    }

    private static int getHash(String title) {
        if (title.length() > 15)
            title = title.substring(0, 15);
        int counter = 0;
        for (int i = 0; i < title.length(); i++)
            counter += (int) title.charAt(i);
        return counter % 19;
    }

    public static int getColor(String title) {
        switch (getHash(title)) {
            case 0:
                return R.color.color0;
            case 1:
                return R.color.color1;
            case 2:
                return R.color.color2;
            case 3:
                return R.color.color3;
            case 4:
                return R.color.color4;
            case 5:
                return R.color.color5;
            case 6:
                return R.color.color6;
            case 7:
                return R.color.color7;
            case 8:
                return R.color.color8;
            case 9:
                return R.color.color9;
            case 10:
                return R.color.color10;
            case 11:
                return R.color.color11;
            case 12:
                return R.color.color12;
            case 13:
                return R.color.color13;
            case 14:
                return R.color.color14;
            case 15:
                return R.color.color15;
            case 16:
                return R.color.color16;
            case 17:
                return R.color.color17;
            case 18:
                return R.color.color18;
        }
        return R.color.color0;
    }

    public static int getDrawer(String title) {
        switch (getHash(title)) {
            case 0:
                return R.drawable.circle0;
            case 1:
                return R.drawable.circle1;
            case 2:
                return R.drawable.circle2;
            case 3:
                return R.drawable.circle3;
            case 4:
                return R.drawable.circle4;
            case 5:
                return R.drawable.circle5;
            case 6:
                return R.drawable.circle6;
            case 7:
                return R.drawable.circle7;
            case 8:
                return R.drawable.circle8;
            case 9:
                return R.drawable.circle9;
            case 10:
                return R.drawable.circle10;
            case 11:
                return R.drawable.circle11;
            case 12:
                return R.drawable.circle12;
            case 13:
                return R.drawable.circle13;
            case 14:
                return R.drawable.circle14;
            case 15:
                return R.drawable.circle15;
            case 16:
                return R.drawable.circle16;
            case 17:
                return R.drawable.circle17;
            case 18:
                return R.drawable.circle18;
        }
        return R.drawable.circle0;
    }
}