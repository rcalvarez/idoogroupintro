package com.example.idoogroup.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.idoogroup.model.User;


public class SaveSharedPreference {
    // Values for Shared Preferences
    private static final String LOGGED_IN_USER = "logged_in_user";

    private static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setLoggedInUser(Context context, User user) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putInt(LOGGED_IN_USER, user.getId());
        editor.apply();
    }

    public static int getLoggedInUser(Context context) {
        return getPreferences(context).getInt(LOGGED_IN_USER, 0);
    }

}
